##
## Настройки Nomad для тестового контура.
##
## Перед использованием шаблона замените все строки «%%...%%» правильными значениями!
##
## @link https://company.atlassian.net/wiki/spaces/dev/pages/308052487
##
## @template-link https://gitlab.company.name/backend/project-templates/
## @template-built 28.09.21 20:55:42
##

job "sbp" {
  # ЦОДы, между которыми надо распределить экземпляры заданий.
  datacenters = [
    "dc1"
  ]
  type = "service"

  #
  # Группа заданий, которые должны быть размещены на одном клиенте.
  #
  group "sbp" {
    # Количество запускаемых копий.
    count = 1

    task "sbp-http" {
      driver = "docker"

      config {
        image = "[[ env "CI_REGISTRY_IMAGE"]]:[[ env "CI_COMMIT_REF_NAME" ]]"
        force_pull = true

        auth {
          server_address = "[[ env "CI_REGISTRY" ]]"
          username = "[[ env "NOMAD_GITLAB_USER" ]]"
          password = "[[ env "NOMAD_GITLAB_PASSWORD" ]]"
        }

        port_map {
          http = 80
          http_dmz = 8080
        }

        logging {
          type = "gelf"
          config {
            gelf-address = "udp://${attr.unique.network.ip-address}:12201"
            tag = "${NOMAD_JOB_NAME}"
          }
        }
      }

      template {
        destination = "local/env.file"
        change_mode = "restart"
        env = true
        data = <<EOF
          {{ range ls "config/common" }}
            {{ .Key | toUpper}}={{ .Value |toJSON }}{{ end }}
          {{ range ls "microservices/[[ env "CI_PROJECT_NAME" ]]" }}
            {{ .Key | toUpper}}={{ .Value |toJSON }}{{ end }}
        EOF
      }

      #
      # Интерфейс приложения, доступный из внешнего мира.
      #
      service {
        name = "${NOMAD_JOB_NAME}"
        port = "http"
        tags = [
          "traefik.enable=true",
          "traefik-front.enable=true"
        ]

        #
        # Проверка исправности.
        #
        check {
          name = "${NOMAD_TASK_NAME}"

          # Тип проверки.
          type = "http"

          # URL метода проверки исправности относительно корня.
          path = "/healthcheck"

          # Время ожидания ответа от службы.
          timeout = "3s"

          # Время между проверками.
          interval = "10s"

          check_restart {
            limit = 3
            grace = "90s"
          }
        }
      }

      #
      # Интерфейс приложения для доверенных запросов.
      #
      service {
        name = "${NOMAD_JOB_NAME}-dmz"
        port = "http_dmz"
        tags = [
          "traefik.enable=true"
        ]

        #
        # Проверка исправности.
        #
        check {
          name = "${NOMAD_TASK_NAME} (DMZ)"

          # Тип проверки.
          type = "http"

          # URL метода проверки исправности относительно корня.
          path = "/healthcheck"

          # Время ожидания ответа от службы.
          timeout = "3s"

          # Время между проверками.
          interval = "10s"

          check_restart {
            limit = 3
            grace = "90s"
          }
        }
      }

      #
      # Ограничения на выделяемые аппаратные ресурсы.
      # ЗАПРЕЩАЕТСЯ увеличивать выделяемые ресурсы без согласования с отделом облачной
      # инфраструктуры!
      # ВАЖНО! Здесь не должно выделяться БОЛЬШЕ ресурсов, чем в prod.nomad! Желательно здесь
      # выделять даже МЕНЬШЕ ресурсов. Это даст запас в боевом контуре.
      #
      resources {
        cpu = 300
        memory = 512
        network {
          # Порт, доступный из внешнего мира.
          port "http" {
          }
          # Порт для доверенных запросов.
          port "http_dmz" {
          }
        }
      }
    }
  }
}
