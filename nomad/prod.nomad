##
## Настройки Nomad для боевого контура.
##
## Перед использованием шаблона замените все строки «%%...%%» правильными значениями!
##
## @link https://company.atlassian.net/wiki/spaces/dev/pages/308052487
##
## @template-link https://gitlab.company.team/backend/project-templates/
## @template-built 28.09.21 20:55:42
##

job "sbp" {
  # ЦОДы, между которыми надо распределить экземпляры заданий.
  datacenters = [
    "dc1"
  ]
  type = "service"

  #
  # Настройки распределения экземпляров заданий по ЦОДам.
  #
  spread {
    # Переменная «meta.rdc» содержит номер локации (ЦОДа).
    attribute = "${meta.rdc}"

    # Определяет вес настроек в этом блоке.
    # Возможные значения: от 0 до 100.
    weight = 100
  }

  #
  # Стратегия выкладывания обновлений.
  #
  update {
    max_parallel = 1
    canary = 2
    min_healthy_time = "10s"
    auto_revert = true
    # Указывает, что распределение должно считаться исправным, когда все задачи запущены и связанные
    # с ними проверки успешны, и неработоспособным, если какая-либо из задач завершается ошибкой или
    # не все проверки проходят.
    health_check = "checks"
    # Автоматически переключаться на новую версию службы, когда все задания (task) этой задачи (job)
    # перейдут в исправное состояние.
    auto_promote = true
  }

  #
  # Группа заданий, которые должны быть размещены на одном клиенте.
  # https://www.nomadproject.io/docs/job-specification/group.html
  #
  group "sbp" {
    # Количество запускаемых копий.
    count = 2

    task "sbp-http" {
      driver = "docker"
      shutdown_delay = "15s"

      config {
        image = "[[ env "CI_REGISTRY_IMAGE"]]:[[ env "CI_COMMIT_REF_NAME" ]]"
        force_pull = true

        auth {
          server_address = "[[ env "CI_REGISTRY" ]]"
          username = "[[ env "NOMAD_GITLAB_USER" ]]"
          password = "[[ env "NOMAD_GITLAB_PASSWORD" ]]"
        }

        port_map {
          http = 80
          http_dmz = 8080
        }

        logging {
          type = "gelf"
          config {
            gelf-address = "udp://${attr.unique.network.ip-address}:12201"
            tag = "${NOMAD_JOB_NAME}"
          }
        }
      }

      template {
        destination = "local/env.file"
        change_mode = "restart"
        env = true
        data = <<EOF
          {{ range ls "config/common" }}
            {{ .Key | toUpper}}={{ .Value |toJSON }}{{ end }}
          {{ range ls "microservices/[[ env "CI_PROJECT_NAME" ]]" }}
            {{ .Key | toUpper}}={{ .Value |toJSON }}{{ end }}
        EOF
      }

      #
      # Интерфейс приложения, доступный из внешнего мира.
      #
      service {
        name = "${NOMAD_JOB_NAME}"
        port = "http"
        tags = [
          "traefik.enable=true",
          "traefik-front.enable=true",
        ]
        canary_tags = [
          "traefik.enable=true",
          "traefik-front.enable=true"
        ]

        #
        # Проверка исправности.
        #
        check {
          name = "${NOMAD_TASK_NAME}"

          # Тип проверки.
          type = "http"

          # URL метода проверки исправности относительно корня.
          path = "/healthcheck"

          # Время ожидания ответа от службы.
          timeout = "3s"

          # Время между проверками.
          interval = "10s"

          check_restart {
            limit = 3
            grace = "90s"
          }
        }
      }

      #
      # Интерфейс приложения для доверенных запросов.
      #
      service {
        name = "${NOMAD_JOB_NAME}-dmz"
        port = "http_dmz"
        tags = [
          "traefik.enable=true"
        ]
        canary_tags = [
          "traefik.enable=true"
        ]

        #
        # Проверка исправности.
        #
        check {
          name = "${NOMAD_TASK_NAME} (DMZ)"

          # Тип проверки.
          type = "http"

          # URL метода проверки исправности относительно корня.
          path = "/healthcheck"

          # Время ожидания ответа от службы.
          timeout = "3s"

          # Время между проверками.
          interval = "10s"

          check_restart {
            limit = 3
            grace = "90s"
          }
        }
      }

      #
      # Ограничения на выделяемые аппаратные ресурсы.
      # ЗАПРЕЩАЕТСЯ увеличивать выделяемые ресурсы без согласования с отделом облачной
      # инфраструктуры!
      # ВАЖНО! Здесь не должно выделяться МЕНЬШЕ ресурсов, чем в test.nomad! Желательно здесь
      # выделять даже БОЛЬШЕ ресурсов. Это даст запас в боевом контуре.
      #
      resources {
        cpu = 300
        memory = 512
        network {
          # Порт, доступный из внешнего мира.
          port "http" {
          }
          # Порт для доверенных запросов.
          port "http_dmz" {
          }
        }
      }
    }
  }
}
