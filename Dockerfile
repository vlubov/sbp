##
## Конфигурация образа Docker.
##

####################################################################################################
## Образ для использования в контурах.
####################################################################################################

FROM company/php:7.4-production AS production

WORKDIR /www_root

COPY ./docker/ /
COPY --chown=nginx:nginx . /www_root

RUN set -eux; \
    chmod a+x bin/*; \
    mkdir -p -m777 /www_root/var; \
    composer install --no-dev --no-interaction --no-scripts

####################################################################################################
## Образ для локальной отладки.
## См. docker-compose.yml.
####################################################################################################

FROM company/php:7.4-debug AS debug

WORKDIR /www_root

COPY ./docker/ /
COPY --from=production /www_root /www_root

####################################################################################################
## Образ по умолчанию.
####################################################################################################

FROM production
