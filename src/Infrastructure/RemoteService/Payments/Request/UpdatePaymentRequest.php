<?php

declare(strict_types=1);

namespace App\Infrastructure\RemoteService\Payments\Request;

/**
 * Данные для запроса на обновление платежа.
 */
class UpdatePaymentRequest
{
    /**
     * Идентификатор платежа.
     */
    public string $id;

    /**
     * Описание состояния.
     */
    public string $result;

    /**
     * Идентификатор транзакции в системе партнера.
     */
    public ?string $externalId;

    /**
     * Статус на который обновляем.
     */
    public string $state;

    /**
     * Код партнера.
     */
    public ?string $partner;

    /**
     * Идентификатор транзакции.
     */
    public string $transaction;

    /**
     * Конструктор класса.
     *
     * @param string      $id          Идентификатор платежа.
     * @param string      $state       Статус на который обновляем.
     * @param string|null $externalId  Идентификатор транзакции в системе партнера.
     * @param string      $result      Описание состояния.
     * @param string|null $partner     Код партнера.
     * @param string      $transaction Идентификатор транзакции.
     */
    public function __construct(
        string $id,
        string $state,
        ?string $externalId,
        string $result,
        ?string $partner,
        string $transaction
    ) {
        $this->id = $id;
        $this->state = $state;
        $this->result = $result;
        $this->externalId = $externalId;
        $this->partner = $partner;
        $this->transaction = $transaction;
    }
}
