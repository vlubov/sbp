<?php

declare(strict_types=1);

namespace App\Infrastructure\RemoteService\Payments\Request;

/**
 * Объект хранящий в себе данные для запроса на создание платежа.
 */
class CreatePaymentRequest
{
    /**
     * Номер договора.
     */
    public string $contract;

    /**
     * Уникальный номер клиента.
     */
    public string $pin;

    /**
     * Эл. почта клиента.
     */
    public string $email;

    /**
     * Мобильный телефон.
     */
    public string $phone;

    /**
     * Сумма платежа.
     */
    public float $amount;

    /**
     * Состояние платежа.
     */
    public string $state;

    /**
     * Комиссия.
     */
    public ?float $commission;

    /**
     * Бонусы.
     */
    public ?int $bonuses;

    /**
     * Создает представление.
     *
     * @param string   $contract   Номер договора.
     * @param string   $pin        Уникальный номер клиента.
     * @param string   $email      Эл. почта клиента.
     * @param string   $phone      Мобильный телефон.
     * @param int      $amount     Сумма платежа.
     * @param string   $state      Состояние платежа.
     * @param int|null $commission Комиссия.
     * @param int|null $bonuses    Бонусы.
     */
    public function __construct(
        string $contract,
        string $pin,
        string $email,
        string $phone,
        int $amount,
        string $state,
        ?int $commission = null,
        ?int $bonuses = null
    ) {
        $this->contract = $contract;
        $this->pin = $pin;
        $this->email = $email;
        $this->phone = $phone;
        $this->amount = $amount;
        $this->state = $state;
        $this->commission = $commission;
        $this->bonuses = $bonuses;
    }
}
