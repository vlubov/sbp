<?php

declare(strict_types=1);

namespace App\Infrastructure\RemoteService\Payments;

use App\Infrastructure\RemoteService\Client\BasicClient;
use App\Infrastructure\RemoteService\Client\Exception\AccessDenied;
use App\Infrastructure\RemoteService\Client\Exception\InternalClientErrorException;
use App\Infrastructure\RemoteService\Client\Exception\RemoteServiceException;
use App\Infrastructure\RemoteService\Client\Exception\UnexpectedResultException;
use App\Infrastructure\RemoteService\Payments\Request\CreatePaymentRequest;
use App\Infrastructure\RemoteService\Payments\Request\UpdatePaymentRequest;
use Psr\Log\LoggerInterface;
use Company\S2SClient\Authentication\Exception\AuthenticationDenied;
use Company\S2SClient\Authentication\Exception\NetworkError;
use Company\S2SClient\Authentication\Exception\ServerError;

/**
 * Клиент для работы с payments.
 */
class PaymentsAdapter
{
    /**
     * Тип ресурса при обращении к службе платежей.
     */
    private const TYPE = 'Payment';

    /**
     * Клиент для отправки запроса.
     */
    private BasicClient $client;

    /**
     * Служба логирования
     */
    private LoggerInterface $logger;

    /**
     * Url обращения к службе.
     */
    private string $url;

    /**
     * Конструктор адаптера.
     *
     * @param BasicClient     $client Клиент.
     * @param string          $url    Url обращения к службе.
     * @param LoggerInterface $logger Служба логирования.
     */
    public function __construct(
        BasicClient $client,
        string $url,
        LoggerInterface $logger
    ) {
        $this->client = $client;
        $this->url = $url;
        $this->logger = $logger;
    }

    /**
     * Отправляет запрос на создание платежа.
     *
     * @param CreatePaymentRequest $request Данные для создания платежа.
     *
     * @return string
     *
     * @throws AuthenticationDenied         Если сервер аутентификации отверг учётные данные.
     * @throws NetworkError                 В случае сетевых ошибок.
     * @throws ServerError                  В случае ошибок на стороне сервера аутентификации.
     * @throws RemoteServiceException       Ошибка на стороне удалённой службы.
     * @throws InternalClientErrorException Внутренняя ошибка клиента.
     * @throws AccessDenied                 Доступ запрещён.
     * @throws UnexpectedResultException    Ошибка если неожиданный результат.
     * @throws \InvalidArgumentException    Неверные аргументы запроса.
     */
    public function createPayment(CreatePaymentRequest $request): string
    {
        $body =
            [
                'data' =>
                    [
                        'type' => self::TYPE,
                        'attributes' => [
                            'amount' => $request->amount / 100,
                            'commission' => $request->commission ? $request->commission / 100 : null,
                            'method' => [
                                'type' => 'sbp',
                                'subtype' => 'interactive'
                            ],
                            'destination' => [
                                'id' => $request->contract,
                                'type' => 'contract',
                                'pin' => $request->pin,
                            ],
                            'source' => [
                                'id' => $request->phone,
                                'type' => 'sbp'
                            ],
                            'payer' => [
                                'pin' => $request->pin,
                                'email' => $request->email,
                                'phone' => $request->phone,
                            ],
                            'partner' => null,
                            'state' => $request->state,
                            'result' => 'Формирование платежа для СБП.'
                        ],
                    ]
                ];
        if ($request->bonuses > 0) {
            $body['included'] = [
                'type' => self::TYPE,
                'attributes' => [
                    'amount' => (float) ($request->commission / 100),
                    'method' => [
                        'type' => 'bonuses'
                    ]
                ]
            ];
        }

        $response = $this->client->sendRequest('POST', $body, $this->url . '/v3/payments');

        if (!isset($response['data']['id'])) {
            $this->logger->error(
                'От службы payments вернулся неизвестный ответ.'
            );

            throw new \LogicException(
                'Неизвестный ответ от службы платежей.',
                0
            );
        }

        return (string) $response['data']['id'];
    }


    /**
     * Отправляет запрос на обновление платежа.
     *
     * @param UpdatePaymentRequest $request Запрос на обновление платежа.
     *
     * @return void
     *
     * @throws AuthenticationDenied         Если сервер аутентификации отверг учётные данные.
     * @throws NetworkError                 В случае сетевых ошибок.
     * @throws ServerError                  В случае ошибок на стороне сервера аутентификации.
     * @throws RemoteServiceException       Ошибка на стороне удалённой службы.
     * @throws InternalClientErrorException Внутренняя ошибка клиента.
     * @throws AccessDenied                 Доступ запрещён.
     * @throws UnexpectedResultException    Ошибка если неожиданный результат.
     * @throws \InvalidArgumentException    Неверные аргументы запроса.
     */
    public function updatePayment(UpdatePaymentRequest $request): void
    {
        $body = [
            'data' => [
                'id' => $request->id,
                'type' => self::TYPE,
                'attributes' => [
                    'state' => $request->state,
                    'result' => $request->result,
                    'partner' => null,
                    // 'partner' => [
                    //     'code' => $request->partner,
                    //     'transactionId' => $request->transaction
                    // ]
                ]
            ]
        ];

        $this->client->sendRequest('PATH', $body, $this->url . \sprintf('/v3/payments/%s', $request->id));
    }
}
