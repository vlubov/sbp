<?php

declare(strict_types=1);

namespace App\Infrastructure\RemoteService\Partner\Request;

/**
 * Запрос на создание короткой ссылки от службы-адаптера для партнера.
 */
class GetSpbLinkRequest
{
    /**
     * Идентификатор платежа.
     */
    public string $paymentId;

    /**
     * Номер договора.
     */
    public string $contract;

    /**
     * Сумма платежа.
     */
    public int $amount;

    /**
     * Время жизни ссылки в минутах.
     */
    public int $ttl;

    /**
     * Конструктор класса.
     *
     * @param string $paymentId Идентификатор платежа.
     * @param string $contract  Номер договора.
     * @param int    $amount    Сумма платежа.
     * @param int    $ttl       Время жизни ссылки в минутах.
     */
    public function __construct(
        string $paymentId,
        string $contract,
        int $amount,
        int $ttl
    ) {
        $this->paymentId = $paymentId;
        $this->contract = $contract;
        $this->amount = $amount;
        $this->ttl = $ttl;
    }
}
