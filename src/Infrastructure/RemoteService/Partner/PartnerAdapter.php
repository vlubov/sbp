<?php

declare(strict_types=1);

namespace App\Infrastructure\RemoteService\Partner;

use App\Infrastructure\RemoteService\Client\BasicClient;
use App\Infrastructure\RemoteService\Client\Exception\AccessDenied;
use App\Infrastructure\RemoteService\Client\Exception\InternalClientErrorException;
use App\Infrastructure\RemoteService\Client\Exception\RemoteServiceException;
use App\Infrastructure\RemoteService\Client\Exception\UnexpectedResultException;
use App\Infrastructure\RemoteService\Partner\Request\GetSpbLinkRequest;
use App\Infrastructure\RemoteService\Partner\Response\BankListResponse;
use App\Infrastructure\RemoteService\Partner\Response\GetSbpLinkResponse;
use Company\JSON\JsonDecoder;
use Company\S2SClient\Authentication\Exception\AuthenticationDenied;
use Company\S2SClient\Authentication\Exception\NetworkError;
use Company\S2SClient\Authentication\Exception\ServerError;

/**
 * Клиент для работы с банком-партнером по СБП.
 */
class PartnerAdapter
{
    /**
     * Клиент для отправки запросов.
     */
    private BasicClient $client;

    /**
     * Активный партнер с которым работаем.
     */
    private string $partner;

    /**
     * URL обращения к службе.
     */
    private string $url;

    /**
     * Создает адаптер.
     *
     * @param BasicClient $client     Клиент для отправки запросов.
     * @param string      $partner    Активный партнер с которым работаем.
     * @param string      $partnerMap Карта партнеров.
     */
    public function __construct(
        BasicClient $client,
        string $partner,
        string $partnerMap
    ) {
        $this->partner = $partner;
        $this->url = JsonDecoder::decode($partnerMap)[$this->partner];
        $this->client = $client;
    }

    /**
     * Запрашивает ссылку у партнера на оплату.
     *
     * @param GetSpbLinkRequest $request Запрос на получение ссылки для оплаты.
     *
     * @return GetSbpLinkResponse
     *
     * @throws AuthenticationDenied         Если сервер аутентификации отверг учётные данные.
     * @throws NetworkError                 В случае сетевых ошибок.
     * @throws ServerError                  В случае ошибок на стороне сервера аутентификации.
     * @throws RemoteServiceException       Ошибка на стороне удалённой службы.
     * @throws InternalClientErrorException Внутренняя ошибка клиента.
     * @throws AccessDenied                 Доступ запрещён.
     * @throws UnexpectedResultException    Ошибка если неожиданный результат.
     * @throws \InvalidArgumentException    Неверные аргументы запроса.
     */
    public function createLink(GetSpbLinkRequest $request): GetSbpLinkResponse
    {
        $body = [
            'data' => [
                'id' => $request->paymentId,
                'type' => 'sbpLink',
                'attributes' => [
                    'contractId' => $request->contract,
                    'amount' => (float) ($request->amount / 100),
                    'ttl' => $request->ttl,
                ]
            ]
        ];

        $response = $this->client->sendRequest('POST', $body, $this->url . '/v1/links');

        if (!isset($response['data']['type']) && $response['data']['type'] !== 'sbpLink') {
            throw new \LogicException(
                sprintf(
                    'Неизвестный ответ от службы работы с партнером %s.',
                    $this->partner,
                )
            );
        }

        return new GetSbpLinkResponse(
            $response['data']['attributes']['amount'],
            $response['data']['attributes']['url'],
            $response['data']['attributes']['qrCode'],
            $response['data']['attributes']['ttl']
        );
    }

    /**
     * Возвращает список банков-партнеров работающих с СБП.
     *
     * @return BankListResponse[]
     *
     * @throws AuthenticationDenied         Если сервер аутентификации отверг учётные данные.
     * @throws NetworkError                 В случае сетевых ошибок.
     * @throws ServerError                  В случае ошибок на стороне сервера аутентификации.
     * @throws RemoteServiceException       Ошибка на стороне удалённой службы.
     * @throws InternalClientErrorException Внутренняя ошибка клиента.
     * @throws AccessDenied                 Доступ запрещён.
     * @throws UnexpectedResultException    Ошибка если неожиданный результат.
     */
    public function getBankList(): array
    {
        $response = $this->client->sendRequest('GET', [], $this->url . '/v1/banks');

        return array_map(
            static fn($data) =>
                new BankListResponse((int) $data['attributes']['bic'], (string) $data['attributes']['name']),
            (array) $response['data']
        );
    }

    /**
     * Возвращает активного банка-партнера СБП.
     */
    public function getPartner(): string
    {
        return $this->partner;
    }
}
