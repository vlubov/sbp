<?php

declare(strict_types=1);

namespace App\Infrastructure\RemoteService\Partner\Response;

/**
 * Ответ с QR - кодом и ссылкой на оплату.
 */
class GetSbpLinkResponse
{
    /**
     * Сумма оплаты.
     */
    public float $amount;

    /**
     * URL - строка.
     */
    public string $url;

    /**
     * QR - код для оплаты.
     */
    public string $qrCode;

    /**
     * Время жизни ссылки в минутах.
     */
    public int $ttl;

    /**
     * Создает конструктор класса.
     *
     * @param float  $amount Сумма платежа в копейках.
     * @param string $url    URL - строка.
     * @param string $qrCode QR - код для оплаты.
     * @param int    $ttl    Время жизни ссылки в минутах.
     */
    public function __construct(
        float $amount,
        string $url,
        string $qrCode,
        int $ttl
    ) {
        $this->amount = $amount;
        $this->url = $url;
        $this->qrCode = $qrCode;
        $this->ttl = $ttl;
    }
}
