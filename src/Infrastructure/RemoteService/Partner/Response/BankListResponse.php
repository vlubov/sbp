<?php

declare(strict_types=1);

namespace App\Infrastructure\RemoteService\Partner\Response;

/**
 * Представление банка.
 */
class BankListResponse
{
    /**
     * БИК банка.
     */
    public int $bic;

    /**
     * Название банка.
     */
    public string $name;

    /**
     * Конструктор класса.
     *
     * @param int    $bic  БИК банка.
     * @param string $name Название банка.
     */
    public function __construct(
        int $bic,
        string $name
    ) {
        $this->bic = $bic;
        $this->name = $name;
    }
}
