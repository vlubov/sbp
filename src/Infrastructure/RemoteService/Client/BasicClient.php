<?php

declare(strict_types=1);

namespace App\Infrastructure\RemoteService\Client;

use App\Infrastructure\RemoteService\Client\Exception\AccessDenied;
use App\Infrastructure\RemoteService\Client\Exception\InternalClientErrorException;
use App\Infrastructure\RemoteService\Client\Exception\RemoteServiceException;
use App\Infrastructure\RemoteService\Client\Exception\UnexpectedResultException;
use App\Infrastructure\RemoteService\Client\Response\Error;
use Psr\Http\Message\ResponseInterface;
use Company\HttpClient\HttpClient;
use Company\JSON\JsonDecoder;
use Company\JSON\JsonEncoder;
use Company\S2SClient\Authentication\Exception\AuthenticationDenied;
use Company\S2SClient\Authentication\Exception\NetworkError;
use Company\S2SClient\Authentication\Exception\ServerError;
use Company\S2SClient\HTTP\Authorization\ServiceToken;

/**
 * Класс-обертка для работы с HTTP клиентом.
 */
class BasicClient
{
    /**
     * Клиент для отправки запросов.
     */
    private HttpClient $client;

    /**
     * Служба авторизации.
     */
    private ServiceToken $serviceToken;

    /**
     * Конструктор класса.
     *
     * @param HttpClient   $client       Клиент.
     * @param ServiceToken $serviceToken Служба авторизации.
     */
    public function __construct(
        HttpClient $client,
        ServiceToken $serviceToken
    ) {
        $this->client = $client;
        $this->serviceToken = $serviceToken;
    }

    /**
     * Формирует стандартный запрос с авторизацией.
     *
     * @param string               $method Тип запроса.
     * @param array<string, mixed> $body   Тело запроса.
     * @param string               $url    Путь по которому обращаемся к службе.
     *
     * @return array<string, mixed>
     *
     * @throws AuthenticationDenied         Если сервер аутентификации отверг учётные данные.
     * @throws NetworkError                 В случае сетевых ошибок.
     * @throws ServerError                  В случае ошибок на стороне сервера аутентификации.
     * @throws RemoteServiceException       Ошибка на стороне удалённой службы.
     * @throws InternalClientErrorException Внутренняя ошибка клиента.
     * @throws AccessDenied                 Доступ запрещён.
     * @throws UnexpectedResultException    Ошибка если неожиданный результат.
     * @throws \InvalidArgumentException    Неверные аргументы запроса.
     */
    public function sendRequest(string $method, array $body, string $url): array
    {
        $request = $this->client
            ->createRequest($method, $url)
            ->withHeader('Content-Type', 'application/json');

        if ($body !== []) {
            $jsonBody = JsonEncoder::encode($body);
            $request = $request->withBody($this->client->createStreamFromString($jsonBody));
        }

        $request = $this->serviceToken->applyTo($request);

        $response = $this->client->sendRequest($request);

        $statusCode = $response->getStatusCode();
        if ($statusCode >= 500) {
            throw new RemoteServiceException(
                sprintf(
                    'Ошибка удаленной службы при вызове "%s %s". %s',
                    $request->getMethod(),
                    $request->getUri(),
                    $this->getTextError($response)
                )
            );
        }

        if ($statusCode === 400) {
            throw new InternalClientErrorException(
                sprintf(
                    'Внутренняя ошибка клиента при вызове "%s %s". %s',
                    $request->getMethod(),
                    $request->getUri(),
                    $this->getTextError($response)
                )
            );
        }

        if ($statusCode === 401 || $statusCode === 403) {
            throw new AccessDenied(
                sprintf(
                    'Нет прав для выполнения "%s %s". %s',
                    $request->getMethod(),
                    $request->getUri(),
                    $this->getTextError($response)
                )
            );
        }

        if ($statusCode === 422) {
            $this->throwExceptionFromResponse($response);
        }

        return $this->convertResponseToArray($response);
    }

    /**
     * Преобразовывает ответ в массив.
     *
     * @param ResponseInterface $response Преобразуемый ответ.
     *
     * @return array<string, mixed>
     *
     * @throws UnexpectedResultException
     */
    private function convertResponseToArray(ResponseInterface $response): array
    {
        try {
            $payload = JsonDecoder::decode((string) $response->getBody());
        } catch (\JsonException $exception) {
            throw new UnexpectedResultException(
                sprintf(
                    'От службы получен ответ, не являющийся JSON. %s',
                    $exception->getMessage()
                ),
                0,
                $exception
            );
        }

        return $payload;
    }

    /**
     * Возвращает список ошибок из тела ответа.
     *
     * @param ResponseInterface $response Ответ от сервера.
     *
     * @return array<Error>
     *
     * @throws UnexpectedResultException
     */
    private function extractErrors(ResponseInterface $response): array
    {
        $payload = $this->convertResponseToArray($response);
        $items = $payload['errors'] ?? [];
        $errors = [];
        foreach ($items as $item) {
            $error = new Error(
                $item['code'] ?? '(код не указан)',
                $item['title'] ?? '(нет описания)',
                $item['detail'] ?? '(нет описания)'
            );
            $errors[] = $error;
        }
        return $errors;
    }

    /**
     * Возвращает текст ошибки.
     *
     * @param ResponseInterface $response Ответ от сервера.
     *
     * @return string
     */
    private function getTextError(ResponseInterface $response): string
    {
        try {
            $errors = $this->extractErrors($response);
        } catch (UnexpectedResultException $exception) {
            return 'Ответ, не являющийся верным JSON.';
        }

        $text = '';
        foreach ($errors as $error) {
            $text .= '[' . $error->getCode() . '] ' . rtrim($error->getDetail(), '.') . '. ';
        }

        return trim($text);
    }

    /**
     * Выбрасывает исключения, в соответствии с полученной ошибкой.
     *
     * @param ResponseInterface $response Ответ службы подписей.
     *
     * @throws UnexpectedResultException
     */
    private function throwExceptionFromResponse(ResponseInterface $response): void
    {
        $errors = $this->extractErrors($response);
        $error = $errors[0] ?? null;

        if (!($error instanceof Error)) {
            throw new UnexpectedResultException('В ответе службы нет перечня ошибок.');
        }
    }
}
