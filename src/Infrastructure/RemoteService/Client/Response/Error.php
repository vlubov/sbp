<?php

declare(strict_types=1);

namespace App\Infrastructure\RemoteService\Client\Response;

/**
 * Сообщение об ошибке.
 */
final class Error
{
    /**
     * Код ошибки.
     */
    private string $code;

    /**
     * Подробное описание.
     */
    private string $detail;

    /**
     * Краткое описание.
     */
    private string $title;

    /**
     * Создаёт сообщение.
     *
     * @param string $code   Код ошибки.
     * @param string $title  Подробное описание.
     * @param string $detail Краткое описание.
     */
    public function __construct(string $code, string $title, string $detail)
    {
        $this->code = $code;
        $this->title = $title;
        $this->detail = $detail;
    }

    /**
     * Возвращает код ошибки.
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * Возвращает подробное описание.
     */
    public function getDetail(): string
    {
        return $this->detail;
    }

    /**
     * Возвращает краткое описание.
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}
