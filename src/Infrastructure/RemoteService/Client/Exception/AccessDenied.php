<?php

declare(strict_types=1);

namespace App\Infrastructure\RemoteService\Client\Exception;

/**
 * Доступ запрещён.
 */
final class AccessDenied extends \RuntimeException
{
}
