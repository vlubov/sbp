<?php

declare(strict_types=1);

namespace App\Infrastructure\RemoteService\Client\Exception;

/**
 * Внутренняя ошибка клиента.
 */
class InternalClientErrorException extends \LogicException
{
}
