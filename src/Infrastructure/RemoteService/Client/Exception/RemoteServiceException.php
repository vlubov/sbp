<?php

declare(strict_types=1);

namespace App\Infrastructure\RemoteService\Client\Exception;

/**
 * Ошибка на стороне удалённой службы.
 */
class RemoteServiceException extends \RuntimeException
{
}
