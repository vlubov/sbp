<?php

declare(strict_types=1);

namespace App\Infrastructure\RemoteService\Client\Exception;

/**
 * Ошибка если неожиданный результат.
 */
final class UnexpectedResultException extends \LogicException
{
}
