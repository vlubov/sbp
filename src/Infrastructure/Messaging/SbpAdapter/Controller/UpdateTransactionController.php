<?php

declare(strict_types=1);

namespace App\Infrastructure\Messaging\SbpAdapter\Controller;

use App\Infrastructure\RemoteService\Payments\PaymentsAdapter;
use App\Infrastructure\RemoteService\Payments\Request\UpdatePaymentRequest;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\Exception\FileLocatorFileNotFoundException;
use Symfony\Component\Config\FileLocatorInterface;
use Company\AmqpConsumer\Controller\BaseMessageController;
use Company\AmqpConsumer\Exception\SchemaNotFound;
use Company\AmqpConsumer\Exception\UnrecoverableException;
use Company\AmqpConsumer\Message\Message;
use Company\JSON\Conversion\JsonConverter;

/**
 * Обработчик состояния оплаты по ссылке.
 */
class UpdateTransactionController extends BaseMessageController
{
    /**
     * Схема валидации сообщения.
     */
    private const SCHEMA_NAME = 'sbp-adapter.payment.state.json';

    /**
     * Средство журналирования.
     *
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * Локатор схем.
     */
    private FileLocatorInterface $schemaLocator;

    /**
     * Служба для работы со службой платежей.
     */
    private PaymentsAdapter $payments;

    /**
     * Конструктор.
     *
     * @param JsonConverter        $jsonConverter Преобразователь JSON.
     * @param LoggerInterface      $logger        Средство журналирования.
     * @param FileLocatorInterface $schemaLocator Локатор схем.
     * @param PaymentsAdapter      $service       Служба для работы со службой платежей.
     */
    public function __construct(
        JsonConverter $jsonConverter,
        LoggerInterface $logger,
        FileLocatorInterface $schemaLocator,
        PaymentsAdapter $service
    ) {
        parent::__construct($jsonConverter, $logger);
        $this->logger = $logger;
        $this->schemaLocator = $schemaLocator;
        $this->payments = $service;
    }

    /**
     * Обновляет состояние платежа.
     *
     * @param Message $message Сообщение.
     *
     * @return void
     * @throws UnrecoverableException
     */
    public function execute(Message $message): void
    {
        $payload = $this->getPayload($message);
        $data = $payload['data'];

        $this->logger->notice(
            sprintf(
                'Обновляем состояния платежа "%s" на "%s".',
                $data['id'],
                $data['attributes']['state'],
            )
        );

        $this->payments->updatePayment(
            new UpdatePaymentRequest(
                $data['id'],
                $payload['data']['attributes']['state'],
                $payload['data']['attributes']['externalId'] ?? null,
                $payload['data']['attributes']['result'] ?? 'Результат оплаты по ссылке.',
                null,
                'Временно не реализованный механизм'
            )
        );
    }

    /**
     * Извлекает тело из сообщения.
     *
     * @param Message $message Обрабатываемое сообщение.
     *
     * @return array<string, mixed>
     * @throws UnrecoverableException
     */
    public function getPayload(Message $message): array
    {
        try {
            return $this->unpackPayload($message);
        } catch (UnrecoverableException $exception) {
            throw new UnrecoverableException(
                sprintf(
                    'Не удалось распаковать сообщение. %s',
                    $exception->getMessage()
                )
            );
        }
    }

    /**
     * Возвращает полный путь к файлу JSON Schema для валидации тела сообщения.
     *
     * @return string
     *
     * @throws SchemaNotFound Если не удалось определить путь к файлу схемы или файл отсутствует.
     *
     * @see unpackPayload()
     */
    protected function getSchemaPath(): string
    {
        try {
            $path = $this->schemaLocator->locate(self::SCHEMA_NAME);
        } catch (\InvalidArgumentException | FileLocatorFileNotFoundException $e) {
            throw new SchemaNotFound(
                sprintf(
                    'Не удалось найти схему из-за ошибки "%s": %s',
                    get_class($e),
                    $e->getMessage()
                )
            );
        }

        return is_array($path) ? $path[0] : $path;
    }
}
