<?php

declare(strict_types=1);

namespace App\Infrastructure\API\HTTP\V1\Error;

use Company\JSON\JsonSchema\ErrorCodes as JsonSchemaCode;

/**
 * Карты соответствия кодов API версии 1.
 */
interface ErrorMap
{
    /**
     * Карта соответствия ошибок JsonSchema ошибкам API.
     */
    public const JSON_SCHEMA_MAP = [
        JsonSchemaCode::DUPLICATE_ITEMS => ErrorCode::SYNTAX_ERROR_IN_REQUEST,
        JsonSchemaCode::FORMAT_MISMATCH => ErrorCode::INVALID_VALUE,
        JsonSchemaCode::GENERIC => ErrorCode::SYNTAX_ERROR_IN_REQUEST,
        JsonSchemaCode::MISSING_PROPERTY => ErrorCode::VALUE_REQUIRED,
        JsonSchemaCode::NOT_ENOUGH_ITEMS => ErrorCode::SYNTAX_ERROR_IN_REQUEST,
        JsonSchemaCode::NOT_ENOUGH_PROPERTIES => ErrorCode::SYNTAX_ERROR_IN_REQUEST,
        JsonSchemaCode::NOT_MULTIPLE_OF => ErrorCode::SYNTAX_ERROR_IN_REQUEST,
        JsonSchemaCode::PATTERN_MISMATCH => ErrorCode::INVALID_VALUE,
        JsonSchemaCode::TOO_MANY_ITEMS => ErrorCode::SYNTAX_ERROR_IN_REQUEST,
        JsonSchemaCode::TOO_MANY_PROPERTIES => ErrorCode::SYNTAX_ERROR_IN_REQUEST,
        JsonSchemaCode::TYPE_MISMATCH => ErrorCode::INVALID_VALUE,
        JsonSchemaCode::UNEXPECTED_PROPERTY => ErrorCode::SYNTAX_ERROR_IN_REQUEST,
        JsonSchemaCode::UNKNOWN_VALUE => ErrorCode::UNKNOWN_VALUE,
        JsonSchemaCode::VALUE_TOO_HIGH => ErrorCode::INVALID_VALUE,
        JsonSchemaCode::VALUE_TOO_LONG => ErrorCode::INVALID_VALUE,
        JsonSchemaCode::VALUE_TOO_LOW => ErrorCode::INVALID_VALUE,
        JsonSchemaCode::VALUE_TOO_SHORT => ErrorCode::INVALID_VALUE,
    ];

    /**
     * Соответствие код ошибки - сообщение об ошибке.
     */
    public const TITLE_MAP = [
        ErrorCode::ACCESS_DENIED => 'Доступ запрещен',
        ErrorCode::AUTHENTICATION_REQUIRED => 'Требуется авторизация',
        ErrorCode::BAD_REQUEST => 'Ошибка в запросе',
        ErrorCode::CONFLICT => 'Запрос не может быть выполнен из-за конфликтного обращения к ресурсу',
        ErrorCode::EXTERNAL_SERVICE_ERROR => 'Ошибка внешней службы',
        ErrorCode::HTTP_METHOD_NOT_ALLOWED => 'Неподдерживаемый метод HTTP',
        ErrorCode::INTERNAL_ERROR => 'Внутренняя ошибка',
        ErrorCode::INVALID_SIGNATURE => 'Неверная подпись',
        ErrorCode::INVALID_VALUE => 'Недопустимое значение',
        ErrorCode::NEED_TO_SIGN_DOCUMENT => 'Не подписан необходимый документ',
        ErrorCode::NOT_FOUND => 'Ресурс не найден',
        ErrorCode::PROFILE_EXISTS => 'Профиль уже зарегистрирован',
        ErrorCode::PROFILE_EXISTS_IN_OTHER_BRAND => 'Профиль уже зарегистрирован в другом бренде',
        ErrorCode::SCHEMA_MISMATCH => 'Запрос не соответствует схеме',
        ErrorCode::SYNTAX_ERROR_IN_REQUEST => 'Синтаксическая ошибка в запросе',
        ErrorCode::TEMPORALLY_INTERNAL_ERROR => 'Временная ошибка внутри службы, необходимо повторить запрос позднее',
        ErrorCode::UNKNOWN_VALUE => 'Неизвестное значение',
        ErrorCode::UNPROCESSABLE_ENTITY => 'Невозможно провести операцию над ресурсом.',
        ErrorCode::UNSUPPORTED_MEDIA_TYPE => 'Неподдерживаемый тип содержимого',
        ErrorCode::VALUE_REQUIRED => 'Требуется значение',
    ];
}
