<?php

declare(strict_types=1);

namespace App\Infrastructure\API\HTTP\V1\Error;

/**
 * Справочник кодов ошибок.
 */
interface ErrorCode
{
    /**
     * Доступ запрещён.
     */
    public const ACCESS_DENIED = 'AccessDenied';

    /**
     * Для выполнения действия требуется аутентификация.
     */
    public const AUTHENTICATION_REQUIRED = 'AuthenticationRequired';

    /**
     * Запрос некорректен.
     */
    public const BAD_REQUEST = 'BadRequest';

    /**
     * Конфликт.
     */
    public const CONFLICT = 'Conflict';

    /**
     * Ошибка во внешней службе.
     */
    public const EXTERNAL_SERVICE_ERROR = 'ExternalServiceError';

    /**
     * Метод HTTP не разрешён для данного ресурса.
     */
    public const HTTP_METHOD_NOT_ALLOWED = 'MethodNotAllowed';

    /**
     * Внутренняя ошибка.
     */
    public const INTERNAL_ERROR = 'InternalError';

    /**
     * Неверная подпись.
     */
    public const INVALID_SIGNATURE = 'InvalidSignature';

    /**
     * Недопустимое значение.
     */
    public const INVALID_VALUE = 'InvalidValue';

    /**
     * Не подписан необходимый документ.
     */
    public const NEED_TO_SIGN_DOCUMENT = 'NeedToSignDocument';

    /**
     * Ресурс не найден.
     */
    public const NOT_FOUND = 'NotFound';

    /**
     * Профиль с такими данными уже зарегистрирован.
     */
    public const PROFILE_EXISTS = 'ProfileExists';

    /**
     * Профиль с такими данными уже зарегистрирован для другого бренда.
     */
    public const PROFILE_EXISTS_IN_OTHER_BRAND = 'ProfileExistsInOtherBrand';

    /**
     * Запрос не соответствует схеме.
     */
    public const SCHEMA_MISMATCH = 'SchemaMismatch';

    /**
     * Синтаксическая ошибка в запросе.
     */
    public const SYNTAX_ERROR_IN_REQUEST = 'SyntaxErrorInRequest';

    /**
     * Временная ошибка внутри службы, необходимо повторить запрос позднее.
     */
    public const TEMPORALLY_INTERNAL_ERROR = 'TemporallyInternalError';

    /**
     * Неизвестное значение.
     */
    public const UNKNOWN_VALUE = 'UnknownValue';

    /**
     * Невозможно провести операцию над ресурсом.
     */
    public const UNPROCESSABLE_ENTITY = 'UnprocessableEntity';

    /**
     * Неподдерживаемый тип содержимого.
     */
    public const UNSUPPORTED_MEDIA_TYPE = 'UnsupportedMediaType';

    /**
     * Требуется значение для указанного свойства.
     */
    public const VALUE_REQUIRED = 'ValueRequired';
}
