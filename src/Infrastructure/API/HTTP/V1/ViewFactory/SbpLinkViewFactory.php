<?php

declare(strict_types=1);

namespace App\Infrastructure\API\HTTP\V1\ViewFactory;

use App\Application\DTO\CreateSbpLinkResponse;
use Company\JsonApiBundle\View\ViewFactory;

/**
 * Фабрика представлений ссылки на оплату.
 */
class SbpLinkViewFactory implements ViewFactory
{
    /**
     * Создает представление.
     *
     * @param string $name     Имя представления.
     * @param mixed  $resource Ресурс, чьё представление надо создать.
     *
     * @return mixed Созданное представление или null, если фабрика не смогла его создать.
     */
    public function createView(string $name, $resource)
    {
        if (!$resource instanceof CreateSbpLinkResponse) {
            return null;
        }

        return [
            'id' => $resource->id,
            'type' => 'sbpLink',
            'attributes' => [
                'amount' => $resource->amount,
                'qrCode' => $resource->qrCode,
                'url' => $resource->url,
                'ttl' => $resource->ttl,
                'partner' => $resource->partner,
            ]
        ];
    }
}
