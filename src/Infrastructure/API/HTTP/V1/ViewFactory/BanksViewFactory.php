<?php

declare(strict_types=1);

namespace App\Infrastructure\API\HTTP\V1\ViewFactory;

use App\Infrastructure\RemoteService\Partner\Response\BankListResponse;
use Company\JsonApiBundle\View\ViewFactory;

/**
 * Представление созданной ссылки на оплату.
 */
class BanksViewFactory implements ViewFactory
{
    /**
     * Создает представление.
     *
     * @param string $name     Имя представления.
     * @param mixed  $resource Ресурс, чьё представление надо создать.
     *
     * @return mixed Созданное представление или null, если фабрика не смогла его создать.
     */
    public function createView(string $name, $resource)
    {
        if ($resource === []) {
            return null;
        }

        return array_map(
            static fn (BankListResponse $bank): array =>
            [
                'id' => (string) $bank->bic,
                'type' => 'SbpBank',
                'attributes' => [
                    'name' => $bank->name,
                    'bic' => (string) $bank->bic,
                    ]
            ],
            (array) $resource
        );
    }
}
