<?php

declare(strict_types=1);

namespace App\Infrastructure\API\HTTP\V1\Controller;

use App\Infrastructure\RemoteService\Client\Exception\AccessDenied;
use App\Infrastructure\RemoteService\Client\Exception\InternalClientErrorException;
use App\Infrastructure\RemoteService\Client\Exception\RemoteServiceException;
use App\Infrastructure\RemoteService\Client\Exception\UnexpectedResultException;
use App\Infrastructure\RemoteService\Partner\PartnerAdapter;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\FileLocatorInterface;
use Symfony\Component\HttpFoundation\Response;
use Company\HttpClient\Exception\RuntimeException;
use Company\Infrastructure\API\HTTP\Error\ErrorCode;
use Company\Infrastructure\API\HTTP\Error\Exception\InternalServerError;
use Company\Infrastructure\API\HTTP\Error\Exception\ServiceUnavailable;
use Company\JsonApiBundle\Controller\JsonApiController;
use Company\S2SClient\Authentication\Exception\AuthenticationDenied;
use Company\S2SClient\Authentication\Exception\NetworkError;
use Company\S2SClient\Authentication\Exception\ServerError;

/**
 * Возвращает список банков.
 */
class GetBankListController extends JsonApiController
{
    /**
     * Служба журналирования.
     */
    private LoggerInterface $logger;

    /**
     * Служба для работы с адаптером для банка-партнера.
     */
    private PartnerAdapter $partner;

    /**
     *  Конструктор класса.
     *
     * @param PartnerAdapter       $partner       Служба для работы с адаптером для банка-партнера.
     * @param FileLocatorInterface $schemaLocator Файловый менеджер.
     * @param LoggerInterface      $logger        Служба журналирования.
     */
    public function __construct(
        PartnerAdapter $partner,
        FileLocatorInterface $schemaLocator,
        LoggerInterface $logger
    ) {
        parent::__construct($schemaLocator);
        $this->partner = $partner;
        $this->logger = $logger;
    }

    /**
     * Запрашивает список банков.
     *
     * @return Response
     *
     * @throws ServiceUnavailable
     * @throws InternalServerError
     */
    public function __invoke(): Response
    {
        try {
            $banks = $this->partner->getBankList();
        } catch (NetworkError | RemoteServiceException | AccessDenied | AuthenticationDenied | ServerError $e) {
            $this->logger->error(
                sprintf(
                    'Получение списка банков-партнеров по СБП завершилось внутренней ошибкой. %s',
                    $e->getMessage()
                )
            );
            throw new ServiceUnavailable(ErrorCode::INTERNAL_ERROR);
        } catch (InternalClientErrorException | \InvalidArgumentException | UnexpectedResultException $e) {
            $this->logger->critical(
                sprintf(
                    'Получение списка банков-партнеров по СБП завершилось критической ошибкой. %s',
                    $e->getMessage()
                )
            );
            throw new InternalServerError(ErrorCode::INTERNAL_ERROR);
        }

        return $this->createJsonApiResponse(['data' => $this->createView('api.http.v1.banks', $banks)]);
    }
}
