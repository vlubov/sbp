<?php

declare(strict_types=1);

namespace App\Infrastructure\API\HTTP\V1\Controller;

use App\Application\DTO\CreateSbpLinkRequest;
use App\Application\Service\CreateSbpLinkService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\FileLocatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Company\Application\Common\Exception\ApplicationLogicException;
use Company\Application\Common\Exception\TemporaryInternalException;
use Company\Infrastructure\API\HTTP\Error\ErrorCode;
use Company\Infrastructure\API\HTTP\Error\Exception\HttpError;
use Company\Infrastructure\API\HTTP\Error\Exception\InternalServerError;
use Company\Infrastructure\API\HTTP\Error\Exception\ServiceUnavailable;
use Company\Infrastructure\API\HTTP\Error\Exception\Unauthorized;
use Company\JsonApiBundle\Controller\JsonApiController;

/**
 * Контроллер создания ссылки на оплату.
 */
class CreateSbpLinkController extends JsonApiController
{
    /**
     * Комиссия за перевод в копейках.
     */
    private int $commission;

    /**
     * Служба получения ссылки на оплату.
     */
    private CreateSbpLinkService $createSbpLinkService;

    /**
     * Служба журналирования.
     */
    private LoggerInterface $logger;

    /**
     * Конструктор класса.
     *
     * @param CreateSbpLinkService $createSbpLinkService Служба получения ссылки на оплату.
     * @param string               $commission           Комиссия за перевод в копейках.
     * @param LoggerInterface      $logger               Служба журналирования.
     * @param FileLocatorInterface $schemaLocator        Файловый менеджер.
     */
    public function __construct(
        CreateSbpLinkService $createSbpLinkService,
        string $commission,
        LoggerInterface $logger,
        FileLocatorInterface $schemaLocator
    ) {
        parent::__construct($schemaLocator);
        $this->createSbpLinkService = $createSbpLinkService;
        $this->logger = $logger;
        $this->commission = (int) $commission;
    }

    /**
     * Создание ссылки на оплату.
     *
     * @param Request $httpRequest Входящий запрос.
     *
     * @return Response
     *
     * @throws ServiceUnavailable
     * @throws Unauthorized
     * @throws InternalServerError
     * @throws HttpError
     */
    public function __invoke(Request $httpRequest): Response
    {
        $payload = $this->extractPayload($httpRequest, 'create.sbp-link.json');
        $attributes = $payload['data']['attributes'];

        try {
            $sbpLink = $this->createSbpLinkService->execute(
                new CreateSbpLinkRequest(
                    $attributes['contract'],
                    $attributes['pin'],
                    $attributes['email'],
                    $attributes['phone'],
                    $attributes['amount'],
                    $this->commission,
                    $attributes['ttl'],
                    $attributes['bonuses'] ?? null
                )
            );
        } catch (TemporaryInternalException $e) {
            $this->logger->error($e->getMessage());
            throw new ServiceUnavailable(ErrorCode::INTERNAL_ERROR);
        } catch (ApplicationLogicException $e) {
            $this->logger->critical($e->getMessage());
            throw new InternalServerError(ErrorCode::INTERNAL_ERROR);
        }

        return $this->createJsonApiResponse(
            ['data' => $this->createView('api.http.v1.createSbpLink', $sbpLink)],
            Response::HTTP_CREATED
        );
    }
}
