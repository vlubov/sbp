<?php

/**
 * Ядро службы.
 *
 */

declare(strict_types=1);

namespace App;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

/**
 * Ядро приложения.
 */
class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    /**
     * Расширения файлов конфигурации.
     */
    private const CONFIG_EXTS = '.{php,xml,yaml,yml}';

    /**
     * Возвращает папку проекта.
     *
     * @return string
     */
    public function getProjectDir(): string
    {
        return \dirname(__DIR__);
    }

    /**
     * Регистрирует пакеты.
     *
     * @return iterable<BundleInterface>
     */
    public function registerBundles(): iterable
    {
        $contents = require $this->getProjectDir() . '/config/bundles.php';
        foreach ($contents as $class => $envs) {
            if ($envs[$this->environment] ?? $envs['all'] ?? false) {
                yield new $class();
            }
        }
    }

    /**
     * Регистрирует компиляторы контейнера.
     *
     * @param ContainerBuilder $container Контейнер.
     */
    protected function build(ContainerBuilder $container): void
    {
        parent::build($container);

        // $container->addCompilerPass(...);
    }

    /**
     * Настраивает контейнер зависимостей.
     *
     * @param ContainerBuilder $container Контейнер.
     * @param LoaderInterface  $loader    Загрузчик.
     *
     * @throws \Exception
     */
    protected function configureContainer(
        ContainerBuilder $container,
        LoaderInterface $loader
    ): void {
        $container->addResource(new FileResource($this->getProjectDir() . '/config/bundles.php'));
        $container->setParameter(
            'container.dumper.inline_class_loader',
            \PHP_VERSION_ID < 70400 || $this->debug
        );
        $container->setParameter('container.dumper.inline_factories', true);
        $confDir = $this->getProjectDir() . '/config';

        $loader->load($confDir . '/{packages}/*' . self::CONFIG_EXTS, 'glob');
        $loader->load(
            $confDir . '/{packages}/' . $this->environment . '/*' . self::CONFIG_EXTS,
            'glob'
        );
        $loader->load($confDir . '/{services}' . self::CONFIG_EXTS, 'glob');
        $loader->load($confDir . '/{services}_' . $this->environment . self::CONFIG_EXTS, 'glob');
    }

    /**
     * Настраивает маршруты.
     *
     * @param RoutingConfigurator $routes Конфигуратор маршрутов.
     */
    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $confDir = $this->getProjectDir() . '/config';

        $routes->import($confDir . '/{routes}/' . $this->environment . '/**/*.yaml');
        $routes->import($confDir . '/{routes}/*.yaml');
        $routes->import($confDir . '/{routes}.yaml');
    }
}
