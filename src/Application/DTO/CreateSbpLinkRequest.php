<?php

declare(strict_types=1);

namespace App\Application\DTO;

/**
 * Запрос на получение ссылки для оплаты.
 */
class CreateSbpLinkRequest
{
    /**
     * Номер договора.
     */
    public string $contract;

    /**
     * Уникальный номер клиента.
     */
    public string $pin;

    /**
     * Эл. почта клиента.
     */
    public string $email;

    /**
     * Номер телефона.
     */
    public string $phone;

    /**
     * Сумма платежа.
     */
    public int $amount;

    /**
     * Комиссия.
     */
    public int $commission;

    /**
     * Время жизни ссылки в минутах.
     */
    public int $ttl;

    /**
     * Бонусы на оплату.
     *
     * @var int|null
     */
    public ?int $bonuses;

    /**
     * Конструктор представления.
     *
     * @param string   $contract   Номер договора.
     * @param string   $pin        Уникальный номер клиента.
     * @param string   $email      Эл. почта клиента.
     * @param string   $phone      Номер телефона.
     * @param int      $amount     Сумма платежа.
     * @param int      $commission Комиссия.
     * @param int      $ttl        Время жизни ссылки в минутах.
     * @param int|null $bonuses    Бонусы на оплату.
     */
    public function __construct(
        string $contract,
        string $pin,
        string $email,
        string $phone,
        int $amount,
        int $commission,
        int $ttl,
        ?int $bonuses
    ) {
        $this->contract = $contract;
        $this->pin = $pin;
        $this->email = $email;
        $this->phone = $phone;
        $this->amount = $amount;
        $this->commission = $commission;
        $this->ttl = $ttl;
        $this->bonuses = $bonuses;
    }
}
