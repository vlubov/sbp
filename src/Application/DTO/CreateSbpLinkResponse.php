<?php

declare(strict_types=1);

namespace App\Application\DTO;

/**
 * Ссылка на оплату.
 */
class CreateSbpLinkResponse
{
    /**
     * Идентификатор платежа.
     */
    public string $id;

    /**
     * Сумма платежа.
     */
    public float $amount;

    /**
     * URL - строка.
     */
    public string $url;

    /**
     * QR - код для оплаты.
     */
    public string $qrCode;

    /**
     * Время жизни ссылки в минутах.
     */
    public int $ttl;

    /**
     * Банк-партнер СБП.
     */
    public string $partner;

    /**
     * Создает конструктор класса.
     *
     * @param string $id      Идентификатор платежа.
     * @param float  $amount  Сумма платежа.
     * @param string $url     URL - строка.
     * @param string $qrCode  QR - код для оплаты.
     * @param int    $ttl     Время жизни ссылки в минутах.
     * @param string $partner Банк-партнер СБП.
     */
    public function __construct(
        string $id,
        float $amount,
        string $url,
        string $qrCode,
        int $ttl,
        string $partner
    ) {
        $this->id = $id;
        $this->amount = $amount;
        $this->url = $url;
        $this->qrCode = $qrCode;
        $this->ttl = $ttl;
        $this->partner = $partner;
    }
}
