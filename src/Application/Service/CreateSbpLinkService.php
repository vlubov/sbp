<?php

declare(strict_types=1);

namespace App\Application\Service;

use App\Application\DTO\CreateSbpLinkRequest;
use App\Application\DTO\CreateSbpLinkResponse;
use App\Application\PaymentState;
use App\Infrastructure\RemoteService\Client\Exception\AccessDenied;
use App\Infrastructure\RemoteService\Client\Exception\InternalClientErrorException;
use App\Infrastructure\RemoteService\Client\Exception\RemoteServiceException;
use App\Infrastructure\RemoteService\Client\Exception\UnexpectedResultException;
use App\Infrastructure\RemoteService\Partner\PartnerAdapter;
use App\Infrastructure\RemoteService\Partner\Request\GetSpbLinkRequest;
use App\Infrastructure\RemoteService\Payments\PaymentsAdapter;
use App\Infrastructure\RemoteService\Payments\Request\CreatePaymentRequest;
use App\Infrastructure\RemoteService\Payments\Request\UpdatePaymentRequest;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Log\LoggerInterface;
use Company\Application\Common\Exception\ApplicationLogicException;
use Company\Application\Common\Exception\TemporaryInternalException;
use Company\Domain\Common\Exception\Communication\MiscommunicationException;
use Company\Domain\Common\Exception\DataStorage\TemporaryDataStorageException;
use Company\Domain\Common\Exception\DataStorage\UnrecoverableDataStorageException;
use Company\HttpClient\Exception\RuntimeException;
use Company\S2SClient\Authentication\Exception\AuthenticationDenied;
use Company\S2SClient\Authentication\Exception\NetworkError;
use Company\S2SClient\Authentication\Exception\ServerError;

/**
 * Служба создания ссылки оплаты.
 */
class CreateSbpLinkService
{
    /**
     * Адаптер к службе платежей.
     */
    private PaymentsAdapter $payments;

    /**
     * Служба для работы с адаптером для банка-партнера.
     */
    private PartnerAdapter $partner;

    /**
     * Служба журналирования.
     */
    private LoggerInterface $logger;

    /**
     * Конструктор класса.
     *
     * @param PaymentsAdapter $payments Адаптер к службе платежей.
     * @param PartnerAdapter  $partner  Служба для работы с партнером.
     * @param LoggerInterface $logger   Служба журналирования.
     */
    public function __construct(
        PaymentsAdapter $payments,
        PartnerAdapter $partner,
        LoggerInterface $logger
    ) {
        $this->payments = $payments;
        $this->partner = $partner;
        $this->logger = $logger;
    }

    /**
     * Получает ссылку на оплату.
     *
     * @param CreateSbpLinkRequest $request Данные для получения ссылки на оплату.
     *
     * @return CreateSbpLinkResponse
     *
     * @throws TemporaryDataStorageException
     * @throws \DomainException
     */
    public function execute(CreateSbpLinkRequest $request): CreateSbpLinkResponse
    {
        try {
            $this->logger->debug(
                sprintf(
                    'Создаем ссылку на платеж суммой "%s" копеек для "%s"...',
                    $request->amount,
                    $request->phone
                )
            );
            $paymentId = $this->payments->createPayment(
                new CreatePaymentRequest(
                    $request->contract,
                    $request->pin,
                    $request->email,
                    $request->phone,
                    $request->amount,
                    PaymentState::PROCESSING,
                    $request->commission,
                    $request->bonuses
                )
            );
            $this->logger->debug(sprintf('Создан платеж "%s"', $paymentId));
            $this->logger->debug(sprintf('Создаем ссылку на оплату платежа "%s"...', $paymentId));

            $link = $this->partner->createLink(
                new GetSpbLinkRequest(
                    $paymentId,
                    $request->contract,
                    $request->amount,
                    $request->ttl
                )
            );
            $this->logger->debug(sprintf('Ссылка на оплату платежа "%s" успешно создана.', $paymentId));
        } catch (NetworkError | RemoteServiceException | AccessDenied | AuthenticationDenied | ServerError $e) {
            if (isset($paymentId)) {
                $this->payments->updatePayment(
                    new UpdatePaymentRequest(
                        $paymentId,
                        PaymentState::FAILED,
                        null,
                        'Не удалось создать ссылку на оплату.',
                        $this->partner->getPartner(),
                        'Поле временно не используется, ожидается доработка адаптера к партнеру.'
                    )
                );
            }

            throw new TemporaryInternalException(
                sprintf(
                    'При попытке создать ссылку на оплату - возникла внутренняя ошибка. %s',
                    $e->getMessage()
                )
            );
        } catch (
            InternalClientErrorException | \InvalidArgumentException | UnexpectedResultException $e
        ) {
            if (isset($paymentId)) {
                $this->payments->updatePayment(
                    new UpdatePaymentRequest(
                        $paymentId,
                        PaymentState::FAILED,
                        null,
                        'Не удалось создать ссылку на оплату.',
                        $this->partner->getPartner(),
                        'Поле временно не используется, ожидается доработка адаптера к партнеру.'
                    )
                );
            }

            throw new ApplicationLogicException(sprintf('Не удалось создать ссылку на оплату. %s', $e->getMessage()));
        }

        return new CreateSbpLinkResponse(
            $paymentId,
            $link->amount,
            $link->url,
            $link->qrCode,
            $link->ttl,
            $this->partner->getPartner()
        );
    }
}
