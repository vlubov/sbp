<?php

declare(strict_types=1);

namespace App\Application;

/**
 * Справочник состояний платежа.
 */
interface PaymentState
{
    /**
     * Платёж сформирован, готов к отправке.
     */
    public const INITIATED = 'initiated';

    /**
     * Не принят партнером. Платёж отклонён партнёром.
     */
    public const DECLINED = 'declined';

    /**
     * Ожидается ответ по платежу.
     */
    public const PROCESSING = 'processing';

    /**
     * Платёж не может быть выполнен.
     */
    public const FAILED = 'failed';

    /**
     * Платёж проведён.
     */
    public const COMPLETED = 'completed';
}
