<?php

/**
 * Автоматизированные тесты.
 *
 */

declare(strict_types=1);

namespace Tests;

use Company\PHPUnit\HTTP\HttpClientTrait;
use Company\PHPUnit\Logger\LoggerTrait;
use Company\PHPUnit\Company\TimeServiceTrait;

/**
 * Примесь с общей для всех тестов функциональностью.
 */
trait CommonTrait
{
    use HttpClientTrait;
    use LoggerTrait;
    use TimeServiceTrait;

    /**
     * Готовит окружение теста.
     *
     * Метод вызывается неявно.
     *
     * @throws \Throwable
     */
    public function setUpForCommonTrait(): void
    {
        // Задаём неправильный часовой пояс, чтобы выявлять ошибки, связанные со временем.
        date_default_timezone_set('Australia/Melbourne');
    }
}
