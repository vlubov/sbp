<?php

/**
 * Функциональные тесты.
 *
 */

declare(strict_types=1);

namespace Tests\Functional\Console;

/**
 * Результат выполнения консольной команды.
 */
final class CommandResult
{
    /**
     * Код завершения.
     */
    public int $exitCode = 0;

    /**
     * Вывод команды.
     */
    public string $output = '';
}
