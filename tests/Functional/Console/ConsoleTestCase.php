<?php

/**
 * Функциональные тесты.
 *
 */

declare(strict_types=1);

namespace Tests\Functional\Console;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Tests\CustomContainerTrait;
use Tests\Functional\FunctionalTestCase;

/**
 * Основной класс для тестов консольного API.
 */
abstract class ConsoleTestCase extends FunctionalTestCase
{
    use CustomContainerTrait;

    /**
     * Выполняет команду, заданную методом getCommandName().
     *
     * @param string ...$arguments Аргументы команды.
     *
     * @return CommandResult Результат выполнения команды.
     *
     * @throws \Throwable
     */
    protected function executeCommand(...$arguments): CommandResult
    {
        $this->dropInternalCache();

        $input = new StringInput($this->getCommandName() . ' ' . implode(' ', $arguments));
        $output = new BufferedOutput();
        $result = new CommandResult();

        $application = $this->getConsoleApplication();
        $application->setAutoExit(false);

        $result->exitCode = $application->run($input, $output);

        $result->output = $output->fetch();

        $this->dropInternalCache();

        return $result;
    }

    /**
     * Возвращает имя проверяемой команды.
     *
     * Пример: «some:command».
     *
     * @return string
     */
    abstract protected function getCommandName(): string;

    /**
     * Возвращает консольное приложение.
     *
     * @return Application
     *
     * @throws \Throwable
     */
    private function getConsoleApplication(): Application
    {
        self::assertNotNull(self::$kernel);
        return new Application(self::$kernel);
    }
}
