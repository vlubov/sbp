<?php

declare(strict_types=1);

namespace Tests\Functional\Console\AMQP\SbpAdapter;

use App\Application\PaymentState;
use Tests\Functional\Console\AMQP\AmqpSubscriberTestCase;
use Tests\Utils\RemoteService\PaymentUpdateRequestExpectation;
use Company\AmqpConsumer\ExitCodes;
use Company\PHPUnit\Company\RemoteService\KeycloakRequestExpectations;

/**
 * Проверяет обработку обновления состояния платежа.
 */
class UpdateTransactionTest extends AmqpSubscriberTestCase
{
    use KeycloakRequestExpectations;
    use PaymentUpdateRequestExpectation;

    /**
     * Ключ маршрутизации обрабатываемых событий.
     */
    private const ROUTING_KEY = 'sbp-adapter.1.event.transaction.updated';

    /**
     * Проверяет успешную обработку сообщения от службы работы с партнером.
     *
     * @return void
     *
     * @throws \Throwable
     */
    public function testSuccess()
    {
        $paymentId = '123456';
        $status = PaymentState::COMPLETED;

        $this->expectKeycloakTokenRequest(['grant_type' => 'client_credentials']);
        $data = $this->composeData($paymentId);

        $this->updatePayment(
            $paymentId,
            [
                'amount' => 1234.56,
                'contract' => '123456/1',
                'pin' => '111222',
                'email' => 'example@email.com',
                'phone' => '+79997771234',
                'result' => (string) $data['data']['attributes']['result'],
                'commission' => 0,
                ],
            $status
        );


        $result = $this->emulateEvent(self::ROUTING_KEY, $data);

        self::assertEquals(ExitCodes::SUCCESS, $result);
    }

    /**
     * Создает данные сообщения.
     *
     * @param string $paymentId Идентификатор платежа.
     *
     * @return array<string, mixed>
     *
     * @throws \Throwable
     */
    private function composeData(string $paymentId): array
    {
        return [
            'meta' => [
                'occurredAt' => $this->getTimeService()->getCurrentTime()->format(DATE_RFC3339),
            ],
            'data' => [
                'id' => $paymentId,
                'type' => 'Payment',
                'attributes' => [
                    'state' => PaymentState::COMPLETED,
                    'externalId' => '1234-5678-ABCD-0000',
                    'result' => 'Результат оплаты.'
                ]
            ]
        ];
    }
}
