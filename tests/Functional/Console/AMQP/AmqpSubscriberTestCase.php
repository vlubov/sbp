<?php

declare(strict_types=1);

namespace Tests\Functional\Console\AMQP;

use Tests\Functional\Console\ConsoleTestCase;
use Company\JSON\JsonEncoder;

/**
 * Основной класс для тестов подписки на сообщения AMQP.
 */
abstract class AmqpSubscriberTestCase extends ConsoleTestCase
{
    /**
     * Эмулирует внешнее событие.
     *
     * @param string               $routingKey Ключ маршрутизации.
     * @param array<string, mixed> $body       Тело сообщения.
     * @param array<string, mixed> $headers    Заголовки сообщения.
     *
     * @return int Код завершения.
     *
     * @throws \Throwable
     */
    protected function emulateEvent(string $routingKey, array $body, array $headers = []): int
    {
        $result = $this->executeCommand(
            '--routing-key',
            $routingKey,
            '--body',
            base64_encode(JsonEncoder::encode($body)),
            '--headers',
            base64_encode(JsonEncoder::encode($headers))
        );

        self::assertEmpty($result->output, $result->output);

        return $result->exitCode;
    }

    /**
     * Возвращает имя проверяемой команды.
     *
     * @return string
     */
    protected function getCommandName(): string
    {
        return 'amqp:consume';
    }
}
