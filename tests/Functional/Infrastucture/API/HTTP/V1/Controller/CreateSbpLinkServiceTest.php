<?php

declare(strict_types=1);

namespace Tests\Functional\Infrastucture\API\HTTP\V1\Controller;

use Psr\Http\Message\ResponseInterface;
use Tests\Functional\Infrastucture\API\HTTP\HttpTestCase;
use Tests\Utils\RemoteService\PaymentUpdateRequestExpectation;
use Tests\Utils\RemoteService\SbpPartnerRequestExpectation;
use Company\PHPUnit\Company\RemoteService\KeycloakRequestExpectations;

/**
 * Проверяет создание ссылки на оплату.
 */
class CreateSbpLinkServiceTest extends HttpTestCase
{
    use KeycloakRequestExpectations;
    use PaymentUpdateRequestExpectation;
    use SbpPartnerRequestExpectation;

    /**
     * Тестирует создание ссылки.
     *
     * @return void
     */
    public function testCreateLink()
    {
        $body = [
            'type' => 'sbpLink',
            'attributes' => [
                'amount' => 123456,
                'pin' => '111222',
                'email' => 'example@mail.com',
                'contract' => '111222/1',
                'phone' => '+79991237766',
                'ttl' => 15,
            ]
        ];
        $id = '123456';
        $data = [
            'amount' => $body['attributes']['amount'],
            'pin' => $body['attributes']['pin'],
            'email' => $body['attributes']['email'],
            'contract' => $body['attributes']['contract'],
            'phone' => $body['attributes']['phone'],
            'ttl' => $body['attributes']['ttl'],
            'result' => 'Ожидается оплата.',
            'commission' => 0,
        ];

        $this->expectKeycloakTokenRequest(['grant_type' => 'client_credentials']);

        $this->createPayment($id, $data);
        $this->createSBPLink($id, $data);

        $response = $this->execute($body);

        $this->validateResponseAgainstScheme(
            $this->getDocPath(),
            '/links',
            'POST',
            $response
        );
        self::assertResponseStatusIsCreated($response);
    }

    /**
     * Выполнение запроса.
     *
     * @param array<string, mixed> $body Тело запроса.
     *
     * @return ResponseInterface
     */
    private function execute(array $body): ResponseInterface
    {
        return $this->sendHttpRequest(
            'POST',
            '/v1/links',
            ['data' => $body],
            [],
        );
    }
}
