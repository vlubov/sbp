<?php

declare(strict_types=1);

namespace Tests\Functional\Infrastucture\API\HTTP\V1\Controller;

use Psr\Http\Message\ResponseInterface;
use Tests\Functional\Infrastucture\API\HTTP\HttpTestCase;
use Tests\Utils\RemoteService\SbpPartnerRequestExpectation;
use Company\PHPUnit\Company\RemoteService\KeycloakRequestExpectations;
use Company\Security\Subject\Subject;

/**
 * Тестирует получение списка банков-партнеров.
 */
class GetBanksListTest extends HttpTestCase
{
    use KeycloakRequestExpectations;
    use SbpPartnerRequestExpectation;

    /**
     * Проверяет успешную работу контроллера.
     */
    public function testGetList(): void
    {
        $this->expectKeycloakTokenRequest(['grant_type' => 'client_credentials']);
        $this->getBanks();

        $response = $this->execute();

        self::validateResponseAgainstScheme(
            $this->getDocPath(),
            '/banks',
            'GET',
            $response
        );
        self::assertResponseStatusIsOk($response);
    }

    /**
     * Выполнение запроса.
     *
     * @return ResponseInterface
     *
     * @throws \JsonException
     * @throws \Throwable
     */
    private function execute(): ResponseInterface
    {
        return $this->sendHttpRequest(
            'GET',
            '/v1/banks',
            [],
            [],
        );
    }
}
