<?php

/**
 * Функциональные тесты.
 *
 */

declare(strict_types=1);

namespace Tests\Functional\Infrastucture\API\HTTP;

use Tests\Functional\FunctionalTestCase;
use Company\PHPUnit\HTTP\HttpResponseAssertsTrait;
use Company\PHPUnit\Company\RemoteService\KeycloakRequestExpectations;

/**
 * Основной класс для тестов HTTP API.
 */
abstract class HttpTestCase extends FunctionalTestCase
{
    use HttpResponseAssertsTrait;
    use KeycloakRequestExpectations;
}
