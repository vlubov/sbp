<?php

/**
 * Функциональные тесты.
 *
 */

declare(strict_types=1);

namespace Tests\Functional;

use Tests\CommonTrait;
use Tests\CustomContainerTrait;
use Company\PHPUnit\HTTP\OpenApiValidatorTrait;
use Company\PHPUnit\Symfony\SymfonyHttpTestCase;

/**
 * Основной класс для функциональных тестов.
 */
abstract class FunctionalTestCase extends SymfonyHttpTestCase
{
    use CommonTrait;
    use CustomContainerTrait;
    use OpenApiValidatorTrait;

    /**
     * Сбрасывает внутренний кэш приложения.
     *
     * @return void
     *
     * @throws \Throwable
     */
    protected function dropInternalCache(): void
    {
    }

    /**
     * Возвращает путь к файлу документации.
     *
     * @param string $version Номер версии.
     * @param string $api     Апи (http или amqp).
     *
     * @return string
     *
     * @throws \Throwable
     */
    protected function getDocPath(string $version = 'v1', string $api = 'http'): string
    {
        $root = dirname(__FILE__, 3);

        return sprintf('%s/docs/%s/%s.yaml', $root, $api, $version);
    }

    /**
     * Возвращает настраиваемый параметр.
     *
     * @param string $parameter Имя параметра.
     *
     * @return mixed
     *
     * @throws \Throwable
     */
    protected function getParameter(string $parameter)
    {
        return $this->getContainer()->get('service_container')->getParameter($parameter);
    }
}
