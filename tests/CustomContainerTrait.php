<?php

/**
 * Автоматизированные тесты.
 *
 */

declare(strict_types=1);

namespace Tests;

use Company\JWT\Key\Key;
use Company\JWT\Key\Keychain\InMemoryKeychain;
use Company\JWT\Key\Keychain\Keychain;

/**
 * Пример примеси с особенностями контейнера зависимостей этого приложения.
 */
trait CustomContainerTrait
{
    /**
     * Готовит окружение теста.
     *
     * Метод вызывается неявно.
     *
     * @throws \Throwable
     */
    public function setUpForCustomContainerTrait(): void
    {
        $container = $this->getContainer();

        $inMemoryKeychain = new InMemoryKeychain();
        $inMemoryKeychain->add(new Key('$id', '$contents', '$algorithm'));
        $container->override(Keychain::class, $inMemoryKeychain);
    }
}
