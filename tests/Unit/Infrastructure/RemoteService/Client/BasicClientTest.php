<?php

declare(strict_types=1);

namespace Tests\Unit\Infrastructure\RemoteService\Client;

use App\Infrastructure\RemoteService\Client\BasicClient;
use App\Infrastructure\RemoteService\Client\Exception\AccessDenied;
use App\Infrastructure\RemoteService\Client\Exception\InternalClientErrorException;
use App\Infrastructure\RemoteService\Client\Exception\RemoteServiceException;
use App\Infrastructure\RemoteService\Client\Exception\UnexpectedResultException;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Tests\Unit\UnitTestCase;
use Company\JSON\JsonEncoder;
use Company\S2SClient\Authentication\AccessTokenProvider;
use Company\S2SClient\HTTP\Authorization\ServiceToken;

/**
 * Проверяет работу клиента для запросов к удаленным службам.
 */
class BasicClientTest extends UnitTestCase
{
    /**
     * Поставщик ключей доступа.
     *
     * @var AccessTokenProvider&MockObject
     */
    private AccessTokenProvider $accessProvider;

    /**
     * Тестируемый клиент.
     */
    private BasicClient $basicClient;

    /**
     * Набор данных для тестирования успешных запросов.
     *
     * @return array<string, array<string, mixed>>
     */
    public static function requestsData(): array
    {
        return [
            'POST' => [
                'method' => 'POST',
                'body' => [
                    'data' => [
                        'id' => 12345,
                        'type' => 'ResourceType',
                        'attributes' => [
                            'attribute1' => 'value'
                        ]
                    ]
                ],
                'responseBody' => [
                    'data' => [
                        'id' => 12345,
                        'type' => 'ResourceType',
                        'attributes' => [
                            'AnswerAttribute' => 'AnswerValue'
                        ]
                    ]
                ],
                'code' => 201
            ],
            'GET' => [
                'method' => 'GET',
                'body' => [],
                'responseBody' => [],
                'code' => 200
            ]
        ];
    }

    /**
     * Проверяет успешный кейс.
     *
     * @param string               $method       Вид запроса.
     * @param array<string, mixed> $body         Тело запроса.
     * @param array<string, mixed> $responseBody Ожидаемое тело ответа.
     * @param int                  $code         Код ответа.
     *
     * @return void
     *
     * @dataProvider requestsData
     */
    public function testSuccess(
        string $method,
        array $body,
        array $responseBody,
        int $code
    ): void {
        $url = 'www.testuri.ru';
        $token = uniqid();

        $request = $this->getHttpClient()
            ->expectRequest($method, $url)
            ->headers(
                [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token
                ]
            );

        if ($body !== []) {
            $request = $request->body($body);
        }
        $request
            ->willReturn(JsonEncoder::encode($responseBody), $code);

        $this->accessProvider
            ->expects(self::once())
            ->method('getToken')
            ->willReturn($token);

        $response = $this->basicClient->sendRequest($method, $body, $url);

        if ($responseBody !== []) {
            self::assertIsArray($response);
            self::assertEquals($response, $responseBody);
        }
    }

    /**
     * Набор ответов от сервера с последующей ожидаемой реакцией.
     *
     * @return array<int, array<string, int|string>>
     */
    public static function badResponseCodeData(): array
    {
        return [
            '400' => [
                'code' => 400,
                'exception' => InternalClientErrorException::class,
            ],
            '401' => [
                'code' => 401,
                'exception' => AccessDenied::class,
            ],
            '403' => [
                'code' => 403,
                'exception' => AccessDenied::class,
            ],
            '422' => [
                'code' => 422,
                'exception' => UnexpectedResultException::class,
            ],
            '500' => [
                'code' => 500,
                'exception' => RemoteServiceException::class,
            ],
            '503' => [
                'code' => 503,
                'exception' => RemoteServiceException::class,
            ],
        ];
    }

    /**
     * Тестирует обработку ответа с кодом проблемы.
     *
     * @param int                      $code      Код ответа.
     * @param class-string<\Throwable> $exception Ожидаемое исключение.
     *
     * @return void
     *
     * @dataProvider badResponseCodeData
     */
    public function testBadResponseCode(
        int $code,
        string $exception
    ): void {
        $url = 'www.testuri.ru';
        $token = uniqid();
        $method = 'GET';

        $request = $this->getHttpClient()
            ->expectRequest($method, $url)
            ->headers(
                [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token
                ]
            );

        $request
            ->willReturn(JsonEncoder::encode([]), $code);

        $this->accessProvider
            ->expects(self::once())
            ->method('getToken')
            ->willReturn($token);

        self::expectException($exception);

        $this->basicClient->sendRequest($method, [], $url);
    }

    /**
     * Готовит окружение теста.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->accessProvider = $this->createMock(AccessTokenProvider::class);
        $this->basicClient = new BasicClient($this->getHttpClient(), new ServiceToken($this->accessProvider));
    }
}
