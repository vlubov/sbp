<?php

declare(strict_types=1);

namespace Tests\Unit\Infrastructure\RemoteService\Partner;

use App\Infrastructure\RemoteService\Client\BasicClient;
use App\Infrastructure\RemoteService\Client\Exception\RemoteServiceException;
use App\Infrastructure\RemoteService\Partner\PartnerAdapter;
use App\Infrastructure\RemoteService\Partner\Request\GetSpbLinkRequest;
use App\Infrastructure\RemoteService\Partner\Response\BankListResponse;
use App\Infrastructure\RemoteService\Partner\Response\GetSbpLinkResponse;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use Tests\Unit\UnitTestCase;
use Company\JSON\JsonDecoder;
use Company\JSON\JsonEncoder;

/**
 * Тестирует работу адаптера к службе-прослойки между партнером.
 */
class PartnerClientTest extends UnitTestCase
{
    /**
     * Код партнера.
     */
    private const PARTNER = 'sbp-rsb';

    /**
     * Карта партнеров.
     */
    private const PARTNER_MAP = '{"sbp-rsb": "https://sbp-rsb.consul.com"}';

    /**
     * Тестируемый адаптер.
     *
     * @var PartnerAdapter
     */
    private PartnerAdapter $adapter;

    /**
     * Класс-обертка для работы с HTTP клиентом.
     *
     * @var BasicClient&MockObject
     */
    private BasicClient $client;

    /**
     * Тестирует получение ссылки от партнера.
     *
     * @return void
     */
    public function testGetLink(): void
    {
        $url = JsonDecoder::decode(self::PARTNER_MAP)[self::PARTNER];
        $paymentId = '123456';
        $data = [
            'data' => [
                'id' => $paymentId,
                'type' => 'sbpLink',
                'attributes' => [
                    'amount' => 1234.56,
                    'contractId' => '123456/1',
                    'ttl' => 15,
                ]
            ]
        ];

        $body = [
            'data' => [
                'type' => 'sbpLink',
                'id' => $paymentId,
                'attributes' => [
                    'amount' => $data['data']['attributes']['amount'],
                    'url' => 'https://ploti.dengi.ru/12356',
                    'qrCode' => 'base64format',
                    'ttl' => $data['data']['attributes']['ttl'],
                ],
            ]
        ];

        $this->client
            ->expects(self::once())
            ->method('sendRequest')
            ->with('POST', $data, $url . '/v1/links')
            ->willReturn($body);

        $sbpLink = $this->adapter->createLink(
            new GetSpbLinkRequest(
                $paymentId,
                $data['data']['attributes']['contractId'],
                (int) ($data['data']['attributes']['amount'] * 100),
                $data['data']['attributes']['ttl']
            )
        );

        self::assertInstanceOf(GetSbpLinkResponse::class, $sbpLink);
        self::assertEquals($sbpLink->url, $body['data']['attributes']['url']);
        self::assertEquals($sbpLink->amount, $body['data']['attributes']['amount']);
        self::assertEquals($sbpLink->qrCode, $body['data']['attributes']['qrCode']);
        self::assertEquals($sbpLink->ttl, $body['data']['attributes']['ttl']);
    }

    /**
     * Тестирует получение списка банков.
     *
     * @return void
     */
    public function testGetBanks(): void
    {
        $url = JsonDecoder::decode(self::PARTNER_MAP)[self::PARTNER];
        $body = [
            'data' => [
                [
                    'type' => 'sbpBank',
                    'id' => 12345,
                    'attributes' => [
                        'name' => 'BankName',
                        'bic' => 12345,
                    ],
                ],
                [
                    'type' => 'sbpBank',
                    'id' => 56789,
                    'attributes' => [
                        'name' => 'BankName 2',
                        'bic' => 56789,
                    ],
                ],
            ],
        ];

        $this->client
            ->expects(self::once())
            ->method('sendRequest')
            ->with('GET', [], $url . '/v1/banks')
            ->willReturn($body);

        $bankList = $this->adapter->getBankList();

        foreach ($bankList as $key => $bank) {
            self::assertInstanceOf(BankListResponse::class, $bank);
            self::assertEquals($bank->bic, $body['data'][$key]['id']);
            self::assertEquals($bank->name, $body['data'][$key]['attributes']['name']);
        }
    }

    /**
     * Готовит окружение теста.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->client = $this->createMock(BasicClient::class);
        $this->adapter = new PartnerAdapter($this->client, self::PARTNER, self::PARTNER_MAP);
    }
}
