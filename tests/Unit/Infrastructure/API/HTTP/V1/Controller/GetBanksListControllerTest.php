<?php

declare(strict_types=1);

namespace Tests\Unit\Infrastructure\API\HTTP\V1\Controller;

use App\Infrastructure\API\HTTP\V1\Controller\GetBankListController;
use App\Infrastructure\RemoteService\Client\Exception\AccessDenied;
use App\Infrastructure\RemoteService\Client\Exception\InternalClientErrorException;
use App\Infrastructure\RemoteService\Client\Exception\RemoteServiceException;
use App\Infrastructure\RemoteService\Client\Exception\UnexpectedResultException;
use App\Infrastructure\RemoteService\Partner\PartnerAdapter;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Config\FileLocatorInterface;
use Tests\Unit\Infrastructure\API\HTTP\ControllerTestCase;
use Company\Infrastructure\API\HTTP\Error\ErrorCode;
use Company\Infrastructure\API\HTTP\Error\Exception\InternalServerError;
use Company\Infrastructure\API\HTTP\Error\Exception\ServiceUnavailable;
use Company\PHPUnit\Logger\Records;
use Company\S2SClient\Authentication\Exception\AuthenticationDenied;
use Company\S2SClient\Authentication\Exception\NetworkError;
use Company\S2SClient\Authentication\Exception\ServerError;
use Company\ServiceBundle\Controller\HttpController;

/**
 * Тестирует обработку ошибок в контроллере.
 */
class GetBanksListControllerTest extends ControllerTestCase
{
    /**
     * Служба для работы с реализацией взаимодействия с банком-партнером по СБП.
     *
     * @var PartnerAdapter&MockObject
     */
    private $partner;

    /**
     * Файловый менеджер.
     *
     * @var FileLocatorInterface&MockObject
     */
    private $schemaLocator;

    /**
     * Создает контроллер.
     *
     * @return HttpController
     */
    protected function createController(): HttpController
    {
        return new GetBankListController(
            $this->partner,
            $this->schemaLocator,
            $this->getLogger()
        );
    }

    /**
     * Набор возможных исключений и их обработка.
     *
     * @return array<string, array<string, mixed>>
     */
    public function exceptionData(): array
    {
        return [
            'NetworkError' => [
                'exception' => new NetworkError('Сетевая ошибка.'),
                'expectedException' => new ServiceUnavailable(ErrorCode::INTERNAL_ERROR),
                'expectLogMessage' => static fn(Records $records) => $records->error(
                    'Получение списка банков-партнеров по СБП завершилось внутренней ошибкой. Сетевая ошибка.'
                ),
            ],
            'RemoteServiceException' => [
                'exception' => new RemoteServiceException(),
                'expectedException' => new ServiceUnavailable(ErrorCode::INTERNAL_ERROR),
                'expectLogMessage' => static fn(Records $records) => $records->error(
                    'Получение списка банков-партнеров по СБП завершилось внутренней ошибкой. '
                ),
            ],
            'AccessDenied' => [
                'exception' => new AccessDenied('Ошибка авторизации.'),
                'expectedException' => new ServiceUnavailable(ErrorCode::INTERNAL_ERROR),
                'expectLogMessage' => static fn(Records $records) => $records->error(
                    'Получение списка банков-партнеров по СБП завершилось внутренней ошибкой. Ошибка авторизации.'
                ),
            ],
            'AuthenticationDenied' => [
                'exception' => new AuthenticationDenied('Отказ в аутентификации.'),
                'expectedException' => new ServiceUnavailable(ErrorCode::INTERNAL_ERROR),
                'expectLogMessage' => static fn(Records $records) => $records->error(
                    'Получение списка банков-партнеров по СБП завершилось внутренней ошибкой. Отказ в аутентификации.'
                ),
            ],
            'ServerError' => [
                'exception' => new ServerError('Ошибка на стороне сервера аутентификации.'),
                'expectedException' => new ServiceUnavailable(ErrorCode::INTERNAL_ERROR),
                'expectLogMessage' => static fn(Records $records) => $records->error(
                    'Ошибка авторизации. Ошибка на стороне сервера аутентификации.'
                ),
            ],
            'InternalClientErrorException' => [
                'exception' => new InternalClientErrorException('Внутренняя ошибка клиента.'),
                'expectedException' => new InternalServerError(ErrorCode::INTERNAL_ERROR),
                'expectLogMessage' => static fn(Records $records) => $records->error(
                    'Получение списка банков-партнеров по СБП завершилось критической ошибкой.' .
                    ' Внутренняя ошибка клиента.'
                ),
            ],
            \InvalidArgumentException::class => [
                'exception' => new \InvalidArgumentException('Неверные параметры.'),
                'expectedException' => new InternalServerError(ErrorCode::INTERNAL_ERROR),
                'expectLogMessage' => static fn(Records $records) => $records->error(
                    'Получение списка банков-партнеров по СБП завершилось критической ошибкой. Неверные параметры.'
                ),
            ],
            'UnexpectedResultException' => [
                'exception' => new UnexpectedResultException('Неожиданный результат.'),
                'expectedException' => new InternalServerError(ErrorCode::INTERNAL_ERROR),
                'expectLogMessage' => static fn(Records $records) => $records->error(
                    'Получение списка банков-партнеров по СБП завершилось критической ошибкой. Неожиданный результат.'
                ),
            ],
        ];
    }

    /**
     * Тестирует обработку ошибок.
     *
     * @param \Exception $exception         Возникшее исключение.
     * @param \Exception $expectedException Ожидаемая обработка поступившего исключения.
     * @param callable   $expectLogMessage  Ожидаемое сообщение журналирования.
     *
     * @dataProvider exceptionData
     *
     * @return void
     */
    public function testExceptions(
        \Exception $exception,
        \Exception $expectedException,
        callable $expectLogMessage
    ): void {
        $this->partner
            ->expects(self::once())
            ->method('getBankList')
            ->willThrowException($exception);

        $this->expectExceptionObject($expectedException);

        $this->getController()();

        $this->getLogger()->getRecords()
            ->delegate($expectLogMessage);
    }

    /**
     * Готовит окружение теста.
     *
     * @return void
     *
     * @throws \Throwable
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->partner = $this->createMock(PartnerAdapter::class);
        $this->schemaLocator = $this->createMock(FileLocatorInterface::class);
    }
}
