<?php

declare(strict_types=1);

namespace Tests\Unit\Infrastructure\API\HTTP\V1\Controller;

use App\Application\Service\CreateSbpLinkService;
use App\Infrastructure\API\HTTP\V1\Controller\CreateSbpLinkController;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Config\FileLocatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Tests\Unit\Infrastructure\API\HTTP\ControllerTestCase;
use Company\Application\Common\Exception\ApplicationLogicException;
use Company\Application\Common\Exception\TemporaryInternalException;
use Company\Infrastructure\API\HTTP\Error\ErrorCode;
use Company\Infrastructure\API\HTTP\Error\Exception\InternalServerError;
use Company\Infrastructure\API\HTTP\Error\Exception\ServiceUnavailable;
use Company\PHPUnit\Logger\Records;
use Company\ServiceBundle\Controller\HttpController;

/**
 * Тестирует корректную обработку ошибок.
 */
class CreateSbpLinkControllerTest extends ControllerTestCase
{
    /**
     * Название схемы.
     */
    private const SCHEMA_NAME = 'create.sbp-link.json';

    /**
     * Путь до схемы.
     */
    private const SCHEMA_PATH = 'src/Infrastructure/API/HTTP/V1/Resource/schemas/create.sbp-link.json';
    /**
     * Служба для получения ссылки на оплату.
     *
     * @var CreateSbpLinkService&MockObject
     */
    private CreateSbpLinkService $createSbpLinkService;

    /**
     * Файловый менеджер.
     *
     * @var FileLocatorInterface&MockObject
     */
    private FileLocatorInterface $schemaLocator;

    /**
     * Набор возможных исключений и их обработка.
     *
     * @return array<string, array<string, mixed>>
     */
    public static function exceptionData(): array
    {
        return [
            'TemporaryInternalException' => [
                'exception' => new TemporaryInternalException(),
                'expectedException' => new ServiceUnavailable(ErrorCode::INTERNAL_ERROR),
                'expectLogMessage' => static fn (Records $records) =>
                $records->error('При попытке создать платеж - возникла внутренняя ошибка. '),
            ],
            'ApplicationLogicException' => [
                'exception' => new ApplicationLogicException(),
                'expectedException' => new InternalServerError(ErrorCode::INTERNAL_ERROR),
                'expectLogMessage' => static fn (Records $records) =>
                $records->critical('Не удалось создать платеж. '),
            ],
        ];
    }

    /**
     * Тестирует обработку ошибок.
     *
     * @param \Exception $exception         Возникшее исключение.
     * @param \Exception $expectedException Ожидаемая обработка поступившего исключения.
     * @param callable   $expectLogMessage  Ожидаемое сообщение журналирования.
     *
     * @dataProvider exceptionData
     *
     * @return void
     */
    public function testExceptions(
        \Exception $exception,
        \Exception $expectedException,
        callable $expectLogMessage
    ): void {
        $requestData = [
            'data' => [
                'type' => 'sbpLink',
                'attributes' => [
                    'amount' => 123456,
                    'pin' => '111222',
                    'email' => 'example@mail.com',
                    'contract' => '123456/1',
                    'phone' => '+79991237766',
                    'ttl' => 15,
                ]
            ]
        ];

        $this->createSbpLinkService
            ->expects(self::once())
            ->method('execute')
            ->willThrowException($exception);

        $this->schemaLocator->expects(self::once())
            ->method('locate')
            ->with(self::SCHEMA_NAME)
            ->willReturn(
                self::SCHEMA_PATH
            );

        $this->getJsonConverter()
            ->expects(self::once())
            ->method('convert')
            ->with('', self::SCHEMA_PATH)
            ->willReturn($requestData);

        self::expectExceptionObject($expectedException);

        $this->getController()($this->createMock(Request::class));

        $this->getLogger()->getRecords()
            ->delegate(
                $expectLogMessage
            );
    }

    /**
     * Создает контроллер.
     *
     * @return HttpController
     */
    protected function createController(): HttpController
    {
        return new CreateSbpLinkController(
            $this->createSbpLinkService,
            '0',
            $this->getLogger(),
            $this->schemaLocator
        );
    }

    /**
     * Формирует окружение теста.
     *
     * @return void
     *
     * @throws \Throwable
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->createSbpLinkService = $this->createMock(CreateSbpLinkService::class);
        $this->schemaLocator = $this->createMock(FileLocatorInterface::class);
    }
}
