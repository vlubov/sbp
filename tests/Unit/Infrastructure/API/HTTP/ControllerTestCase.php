<?php

declare(strict_types=1);

namespace Tests\Unit\Infrastructure\API\HTTP;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Container\ContainerInterface;
use Tests\Unit\UnitTestCase;
use Company\JSON\Conversion\JsonConverter;
use Company\JsonApiBundle\View\ViewFactory;
use Company\RequestId\Provider\RequestIdProvider;
use Company\ServiceBundle\Controller\HttpController;

/**
 * Основной класс для модульных тестов.
 */
abstract class ControllerTestCase extends UnitTestCase
{
    /**
     * Проверяемый контроллер.
     */
    private ?HttpController $controller = null;

    /**
     * Преобразователь JSON.
     *
     * @var JsonConverter&MockObject
     */
    private JsonConverter $jsonConverter;

    /**
     * Интерфейс поставщика идентификаторов запросов.
     *
     * @var RequestIdProvider&MockObject
     */
    private RequestIdProvider $requestIdProvider;

    /**
     * Фабрика представлений.
     *
     * @var ViewFactory&MockObject
     */
    private ViewFactory $viewFactory;

    /**
     * Возвращает преобразователь JSON.
     *
     * @return JsonConverter&MockObject
     */
    public function getJsonConverter(): JsonConverter
    {
        return $this->jsonConverter;
    }

    /**
     * Возвращает интерфейс поставщика идентификаторов запросов.
     *
     * @return RequestIdProvider&MockObject
     */
    public function getRequestIdProvider(): RequestIdProvider
    {
        return $this->requestIdProvider;
    }

    /**
     * Создаёт проверяемый контролер.
     *
     * @return HttpController
     *
     * @throws \Throwable
     */
    abstract protected function createController(): HttpController;

    /**
     * Возвращает проверяемый контроллер.
     *
     * @return HttpController
     *
     * @throws \Throwable
     */
    protected function getController(): HttpController
    {
        if ($this->controller === null) {
            $container = $this->createMock(ContainerInterface::class);
            $container->method('get')->willReturnMap(
                [
                    ['logger', $this->getLogger()],
                    ['view_factory', $this->getViewFactory()],
                    ['json_converter', $this->getJsonConverter()],
                    ['request_id_provider', $this->getRequestIdProvider()],
                ]
            );

            $this->controller = $this->createController();
            $this->controller->setContainer($container);
        }

        return $this->controller;
    }

    /**
     * Возвращает фабрику представлений.
     *
     * @return ViewFactory&MockObject
     */
    protected function getViewFactory(): ViewFactory
    {
        return $this->viewFactory;
    }

    /**
     * Готовит окружение теста.
     *
     * @throws \Throwable
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->viewFactory = $this->createMock(ViewFactory::class);
        $this->jsonConverter = $this->createMock(JsonConverter::class);
        $this->requestIdProvider = $this->createMock(RequestIdProvider::class);
    }
}
