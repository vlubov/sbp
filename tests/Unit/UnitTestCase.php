<?php

/**
 * Модульные тесты.
 *
 */

declare(strict_types=1);

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Tests\CommonTrait;

/**
 * Тесты основной клас для модульных тестов.
 */
abstract class UnitTestCase extends TestCase
{
    use CommonTrait;
}
