<?php

declare(strict_types=1);

namespace Tests\Unit\Application\CreateSbpLink\Service;

use App\Application\DTO\CreateSbpLinkRequest;
use App\Application\DTO\CreateSbpLinkResponse;
use App\Application\PaymentState;
use App\Application\Service\CreateSbpLinkService;
use App\Infrastructure\RemoteService\Client\Exception\AccessDenied;
use App\Infrastructure\RemoteService\Client\Exception\InternalClientErrorException;
use App\Infrastructure\RemoteService\Client\Exception\RemoteServiceException;
use App\Infrastructure\RemoteService\Client\Exception\UnexpectedResultException;
use App\Infrastructure\RemoteService\Partner\PartnerAdapter;
use App\Infrastructure\RemoteService\Partner\Request\GetSpbLinkRequest;
use App\Infrastructure\RemoteService\Partner\Response\GetSbpLinkResponse;
use App\Infrastructure\RemoteService\Payments\PaymentsAdapter;
use App\Infrastructure\RemoteService\Payments\Request\CreatePaymentRequest;
use App\Infrastructure\RemoteService\Payments\Request\UpdatePaymentRequest;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\Unit\UnitTestCase;
use Company\Application\Common\Exception\ApplicationLogicException;
use Company\Application\Common\Exception\TemporaryInternalException;
use Company\S2SClient\Authentication\Exception\AuthenticationDenied;
use Company\S2SClient\Authentication\Exception\NetworkError;
use Company\S2SClient\Authentication\Exception\ServerError;

/**
 * Тестирует службу по получении ссылки на оплату.
 */
class GetSbpLinkServiceTest extends UnitTestCase
{
    /**
     * Служба для работы с реализацией взаимодействия с банком-партнером по СБП.
     *
     * @var PartnerAdapter&MockObject
     */
    private PartnerAdapter $partner;

    /**
     * Адаптер взаимодействия со службой payments.
     *
     * @var PaymentsAdapter&MockObject
     */
    private PaymentsAdapter $payments;

    /**
     * Тестируемая служба.
     *
     * @var CreateSbpLinkService
     */
    private CreateSbpLinkService $service;

    /**
     * Тестирует успешную работу службы.
     *
     * @return void
     */
    public function testSuccess(): void
    {
        $data = [
            'contract' => '111222/1',
            'pin' => '111222',
            'email' => 'example@mail.com',
            'phone' => '+79991112233',
            'amount' => 123456,
            'commission' => 0,
            'ttl' => 15,
        ];
        $partnerCode = 'sbp-rsb';
        $paymentId = '123456';
        $url = 'www.lenengrag.ru';
        $qrCode = 'base64format';

        $this->payments
            ->expects(self::once())
            ->method('createPayment')
            ->with(
                new CreatePaymentRequest(
                    $data['contract'],
                    $data['pin'],
                    $data['email'],
                    $data['phone'],
                    $data['amount'],
                    PaymentState::PROCESSING,
                    $data['commission'],
                    null
                )
            )
            ->willReturn($paymentId);

        $this->partner
            ->expects(self::once())
            ->method('createLink')
            ->with(
                new GetSpbLinkRequest(
                    $paymentId,
                    $data['contract'],
                    $data['amount'],
                    $data['ttl'],
                )
            )
            ->willReturn(
                new GetSbpLinkResponse(
                    $data['amount'] / 100,
                    $url,
                    $qrCode,
                    $data['ttl']
                )
            );

        $this->partner
            ->expects(self::once())
            ->method('getPartner')
            ->willReturn($partnerCode);

        $result = $this->service->execute(
            new CreateSbpLinkRequest(
                $data['contract'],
                $data['pin'],
                $data['email'],
                $data['phone'],
                $data['amount'],
                0,
                $data['ttl'],
                null
            )
        );

        self::assertInstanceOf(CreateSbpLinkResponse::class, $result);
        self::assertEquals($result->id, $paymentId);
        self::assertEquals($result->partner, $partnerCode);
        self::assertEquals($result->amount, $data['amount'] / 100);
        self::assertEquals($result->ttl, $data['ttl']);
        self::assertEquals($result->url, $url);
        self::assertEquals($result->qrCode, $qrCode);

        $this->getLogger()->getRecords()
            ->debug(
                sprintf(
                    'Создаем ссылку на платеж суммой "%s" копеек для "%s"...',
                    $data['amount'],
                    $data['phone']
                )
            )
            ->debug(sprintf('Создан платеж "%s"', $paymentId))
            ->debug(sprintf('Создаем ссылку на оплату платежа "%s"...', $paymentId))
            ->debug(sprintf('Ссылка на оплату платежа "%s" успешно создана.', $paymentId));
    }

    /**
     * Набор возможных исключений и их обработка.
     *
     * @return array<string, array<string, mixed>>
     */
    public function exceptionData(): array
    {
        return [
            'NetworkError' => [
                'exception' => new NetworkError(),
                'expectedException' =>
                    new TemporaryInternalException(
                        'При попытке создать ссылку на оплату - возникла внутренняя ошибка. '
                    ),
            ],
            'RemoteServiceException' => [
                'exception' => $this->createMock(RemoteServiceException::class),
                'expectedException' =>
                    new TemporaryInternalException(
                        'При попытке создать ссылку на оплату - возникла внутренняя ошибка. '
                    ),
            ],
            'AccessDenied' => [
                'exception' => new AccessDenied(),
                'expectedException' =>
                    new TemporaryInternalException(
                        'При попытке создать ссылку на оплату - возникла внутренняя ошибка. '
                    ),
            ],
            'AuthenticationDenied' => [
                'exception' => new AuthenticationDenied(),
                'expectedException' =>
                    new TemporaryInternalException(
                        'При попытке создать ссылку на оплату - возникла внутренняя ошибка. '
                    ),
            ],
            'ServerError' => [
                'exception' => new ServerError(),
                'expectedException' =>
                    new TemporaryInternalException(
                        'При попытке создать ссылку на оплату - возникла внутренняя ошибка. '
                    )
            ],
            'UnexpectedResultException' => [
                'exception' => new UnexpectedResultException(),
                'expectedException' => new ApplicationLogicException('Не удалось создать ссылку на оплату. ')
            ],
            'InternalClientErrorException' => [
                'exception' => new InternalClientErrorException(),
                'expectedException' => new ApplicationLogicException('Не удалось создать ссылку на оплату. ')
            ],
            \InvalidArgumentException::class => [
                'exception' => new \InvalidArgumentException(),
                'expectedException' => new ApplicationLogicException('Не удалось создать ссылку на оплату. ')
            ],
        ];
    }

    /**
     * Тестирует обработку ошибок.
     *
     * @param \Exception $exception         Возникшее исключение.
     * @param \Exception $expectedException Исключение в результате перехвата.
     *
     * @dataProvider exceptionData
     *
     * @return void
     */
    public function testExceptions(
        \Exception $exception,
        \Exception $expectedException
    ): void {
        $data = [
            'contract' => '111222/1',
            'pin' => '111222',
            'email' => 'example@mail.com',
            'phone' => '+79991112233',
            'amount' => 123456,
            'commission' => 0,
            'ttl' => 15,
        ];
        $paymentId = '123456';

        $this->payments
            ->expects(self::once())
            ->method('createPayment')
            ->with(
                new CreatePaymentRequest(
                    $data['contract'],
                    $data['pin'],
                    $data['email'],
                    $data['phone'],
                    $data['amount'],
                    PaymentState::PROCESSING,
                    $data['commission'],
                    null
                )
            )
            ->willReturn($paymentId);

        $this->partner
            ->expects(self::once())
            ->method('createLink')
            ->willThrowException($exception);

        $this->partner
            ->expects(self::once())
            ->method('getPartner')
            ->willReturn('PartnerCode');

        $this->payments
            ->expects(self::once())
            ->method('updatePayment')
            ->with(
                new UpdatePaymentRequest(
                    $paymentId,
                    PaymentState::FAILED,
                    null,
                    'Не удалось создать ссылку на оплату.',
                    'PartnerCode',
                    'Поле временно не используется, ожидается доработка адаптера к партнеру.'
                )
            );

        $request = new CreateSbpLinkRequest(
            $data['contract'],
            $data['pin'],
            $data['email'],
            $data['phone'],
            $data['amount'],
            $data['commission'],
            $data['ttl'],
            null
        );

        $this->expectExceptionObject($expectedException);

        $this->service->execute($request);
    }

    /**
     * Формирует окружение для тестирования.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->partner = $this->createMock(PartnerAdapter::class);
        $this->payments = $this->createMock(PaymentsAdapter::class);
        $this->service = new CreateSbpLinkService(
            $this->payments,
            $this->partner,
            $this->getLogger()
        );
    }
}
