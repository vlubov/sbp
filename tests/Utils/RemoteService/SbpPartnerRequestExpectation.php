<?php

declare(strict_types=1);

namespace Tests\Utils\RemoteService;

use Company\PHPUnit\HTTP\TestHttpClient;

trait SbpPartnerRequestExpectation
{
    /**
     * Возвращает клиента HTTP.
     */
    abstract public function getHttpClient(): TestHttpClient;

    /**
     * Создает ожидание запроса на создание платежа.
     *
     * @param string               $id   Идентификатор платежа.
     * @param array<string, mixed> $body Данные для запроса.
     *
     * @return void
     * @throws \Throwable
     */
    public function createSBPLink(string $id, array $body): void
    {
        $this->getHttpClient()
            ->expectRequest('POST', 'https://partner-url.consul/v1/links')
            ->body(
                [
                    'data' =>
                        [
                            'id' => $id,
                            'type' => 'sbpLink',
                            'attributes' => [
                                'contractId' => $body['contract'],
                                'amount' => (float) ($body['amount'] / 100),
                                'ttl' => $body['ttl'],
                            ],
                        ]
                ]
            )
        ->headers(['Content-Type' => 'application/json'])
        ->willReturn(
            [
                'data' => [
                    'id' => $id,
                    'type' => 'sbpLink',
                    'attributes' => [
                        'amount' => (float) ($body['amount'] / 100),
                        'url' => 'https://test.Company.com/sbp-rsb/v1',
                        'qrCode' =>
                            'iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4/' .
                            '/8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==',
                        'ttl' => $body['ttl']
                        ],
                    ]
            ],
            201
        );
    }

    /**
     * Создает ожидание запроса на получение банк-партнеров.
     *
     * @param array<string, array<string, mixed>> $banks Список возвращаемых банков.
     *
     * @throws \Throwable
     */
    public function getBanks(array $banks = []): void
    {
        if ($banks === []) {
            $banks = [
                'data' => [
                    [
                        'type' => 'sbpBank',
                        'id' => 12345,
                        'attributes' => [
                            'bic' => 12345,
                            'name' => 'Акционерное общество «Банк Русский Стандарт»'
                        ]
                    ],
                    [
                        'type' => 'sbpBank',
                        'id' => 54321,
                        'attributes' => [
                            'bic' => 54321,
                            'name' => 'Акционерное общество «КИВИ банк»'
                        ]
                    ]
                ]
            ];
        }
        $this->getHttpClient()
            ->expectRequest('GET', 'https://partner-url.consul/v1/banks')
            ->headers(['Content-Type' => 'application/json'])
            ->willReturn(
                $banks
            );
    }
}
