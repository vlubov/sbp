<?php

declare(strict_types=1);

namespace Tests\Utils\RemoteService;

use App\Application\PaymentState;
use Company\PHPUnit\HTTP\TestHttpClient;

trait PaymentUpdateRequestExpectation
{
    /**
     * Возвращает клиента HTTP.
     */
    abstract public function getHttpClient(): TestHttpClient;

    /**
     * Создает ожидание запроса на создание платежа.
     *
     * @param string               $id   Идентификатор платежа.
     * @param array<string, mixed> $body Данные для запроса.
     *
     * @throws \Throwable
     */
    public function createPayment(string $id, array $body): void
    {
        $this->getHttpClient()
            ->expectRequest('POST', 'https://payments.service.consul/v3/payments')
            ->body(
                [
                    'data' =>
                        [
                            'type' => 'Payment',
                            'attributes' => [
                                'amount' => (float) ($body['amount'] / 100),
                                'commission' => $body['commission'] ? (float) ($body['commission'] / 100) : null,
                                'method' => [
                                    'type' => 'sbp',
                                    'subtype' => 'interactive'
                                ],
                                'destination' => [
                                    'id' => $body['contract'],
                                    'type' => 'contract',
                                    'pin' => $body['pin']
                                ],
                                'source' => [
                                    'id' => $body['phone'],
                                    'type' => 'sbp'
                                ],
                                'payer' => [
                                    'pin' => $body['pin'],
                                    'email' => $body['email'],
                                    'phone' => $body['phone'],
                                ],
                                'partner' => null,
                                'state' => PaymentState::PROCESSING,
                                'result' => 'Формирование платежа для СБП.'
                            ],
                        ]
                ]
            )
        ->headers(['Content-Type' => 'application/json'])
        ->willReturn(
            [
                'data' => [
                    'id' => $id,
                    'type' => 'Payment',
                    'attributes' => [
                        'amount' => $body['amount'],
                        'commission' => $body['commission'],
                        'method' => [
                            'type' => 'sbp',
                            'subtype' => 'interactive'
                        ],
                        'partner' => [
                            'code' => 'qiwi',
                            'transactionId' => 'ADR-12345678-23'
                        ],
                        'purpose' => [
                            'id' => $body['contract'],
                            'type' => 'contract'
                        ],
                        'state' => PaymentState::INITIATED,
                        'result' => 'Формирование платежа для СБП.',
                        'createdAt' => '2022-02-24T04:00+03:00',
                        'updatedAt' => '2022-02-24T04:00+03:00',
                    ],
                    'relationships' => [
                        'payments' => [
                            'data' => [
                                'id' => $id,
                                'type' => 'Payment'
                            ]
                        ]
                    ]
                ]
            ],
            201
        );
    }

    /**
     * Создает ожидание запроса на обновление состояния платежа.
     *
     * @param string               $id    Идентификатор запроса.
     * @param array<string, mixed> $body  Данные запроса.
     * @param string               $state Новое состояние платежа.
     *
     * @throws \Throwable
     */
    public function updatePayment(string $id, array $body, string $state): void
    {
        $this->getHttpClient()
            ->expectRequest('PATH', \sprintf('https://payments.service.consul/v3/payments/%s', $id))
            ->body(
                [
                    'data' => [
                        'id' => $id,
                        'type' => 'Payment',
                        'attributes' => [
                            'state' => $state,
                            'result' => $body['result'],
                            'partner' => $body['partner'] ?? null,
                        ]
                    ]
                ]
            )
            ->headers(['Content-Type' => 'application/json'])
            ->willReturn(
                [
                    'data' => [
                        'id' => $id,
                        'type' => 'Payment',
                        'attributes' => [
                            'amount' => $body['amount'],
                            'commission' => $body['commission'],
                            'method' => [
                                'type' => 'sbp',
                                'subtype' => 'interactive'
                            ],
                            'destination' => [
                                'id' => $body['contract'],
                                'type' => 'contract',
                                'pin' => $body['pin'],
                            ],
                            'source' => [
                                'id' => $body['phone'],
                                'type' => 'sbp'
                            ],
                            'payer' => [
                                'pin' => $body['pin'],
                                'email' => $body['email'],
                                'phone' => $body['phone'],
                            ],
                            'state' => $state,
                            'result' => $body['result'],
                            'createdAt' => '2022-02-24T04:00+03:00',
                            'updatedAt' => '2022-02-24T04:00+03:00',
                        ],
                        'relationships' => [
                            'payments' => [
                                'data' => [
                                    'id' => $id,
                                    'type' => 'Payment'
                                ]
                            ]
                        ]
                    ]
                ]
            );
    }
}
