#!/usr/bin/env bash
##
## Тесты журналирования.
##
## Использует метод "/testing/logging".
##
## @link https://company.atlassian.net/wiki/spaces/dev/pages/254542134/HTTP
##
## @template-link https://gitlab.company.name/backend/project-templates/
## @template-built 28.09.21 20:55:42
##

source "$(readlink -f "$(dirname "${BASH_SOURCE[0]}")")/libtesting.bash"

###[ Настройки ]####################################################################################

# URL метода проверки журналирования.
LOGGING_TEST_API_METHOD=${LOGGING_TEST_API_METHOD:='/testing/logging'}

###[ Выполнение тестов ]############################################################################

# Файл Docker для создания образа.
dockerFilePath="${ROOT_DIR}/${DOCKERFILE}"

## Готовим список аргументов для передачи переменных команде docker.
dockerEnvArgs=$(dockerComposeEnvArgs ${envVars})

envVars=$(envVarsFromDotEnv "${ROOT_DIR}")

for var in ${envVars}; do
    dockerEnvArgs="${dockerEnvArgs} -e ${var}"
done

if [[ "${SKIP_BUILD=0}" != "1" ]]; then
    # Собираем тестовый образ.
    if ! docker build --file "${dockerFilePath}" --tag "${DOCKER_IMAGE}" "${ROOT_DIR}"; then
        fatalError 'Не удалось собрать тестовый образ!'
    fi
fi

# Убеждаемся что мы работаем с новым и чистым контейнером.
dockerRemoveContainer "${CONTAINER}"

# Запускаем контейнер.
dockerRunContainer "${CONTAINER}" ${dockerEnvArgs}
dockerWaitContainerStatus "${CONTAINER}" 'running'
dockerWaitContainerUp "${ROOT_URL}"

# Отправляем проверочный запрос.
curl --silent "${ROOT_URL}${LOGGING_TEST_API_METHOD}" 1>/dev/null 2>/dev/null

echo
echo '--[ Docker STDOUT + STDERR ]-------------------------------------------------------------'
docker logs ${CONTAINER} 2>&1
echo '-----------------------------------------------------------------------------------------'
echo

docker logs "${CONTAINER}" 2>&1 | grep "${LOGGING_TEST_TEXT}" | sed 's/\\/\\\\/g' | docker-compose run dev-tools validate logging.v1 --stdin
exitCode=$?

dockerRemoveContainer "${CONTAINER}"

exit ${exitCode}
