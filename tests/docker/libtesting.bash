#!/usr/bin/env bash
##
## Библиотека для тестирования службы.
##
## @template-link https://gitlab.company.name/backend/project-templates/
## @template-built 28.09.21 20:55:42
##

# Папка тестов.
TESTS_DIR=$(readlink -f "$(dirname "${BASH_SOURCE[0]}")")

# Корневая папка проекта.
ROOT_DIR=$(readlink -f "${TESTS_DIR}/../..")

# Подключаем индивидуальные настройки проекта.
if [[ -f "${ROOT_DIR}/docker-tests.conf" ]]; then
    source "${ROOT_DIR}/docker-tests.conf"
fi

###[ Настройки ]####################################################################################

# Имя проекта (используется для именования образа Docker).
# Если не задано будет взято имя корневой папки.
PROJECT=$(echo ${PROJECT:=$(basename "${ROOT_DIR}")} | tr '[:upper:]' '[:lower:]')

# Имя образа Docker.
DOCKER_IMAGE="${PROJECT}:test"

# Имя контейнера Docker.
CONTAINER="${PROJECT}-test"

# Файл Docker для сборки образа.
DOCKERFILE=${DOCKERFILE:='Dockerfile'}

# Хост для подключения к контейнеру.
CONTAINER_HOST=${CONTAINER_HOST:='localhost'}

# URL метода проверки исправности.
HEALTHCHECK_URL=${HEALTHCHECK_URL:='/healthcheck'}

# Порт для подключения к контейнеру.
if [[ "${CONTAINER_PORT=}" == '' ]]; then
    # Если порт не был задан в настройках, задаём самостоятельно.
    CONTAINER_PORT=8080
    # Если порт уже занят...
    while nc -z "${CONTAINER_HOST}" "${CONTAINER_PORT}"; do
        # ...пробуем случайный порт между 8000 и 9000.
        CONTAINER_PORT=$((8000 + RANDOM % 1000))
    done
fi

# Порт, на котором слушает служба (внутри контейнера).
SERVICE_PORT=${SERVICE_PORT:='8080'}

# Аргументы, обязательные при запуске контейнера.
CONTAINER_RUN_ARGS=${CONTAINER_RUN_ARGS:=''}

# Корневой URL службы.
ROOT_URL=${ROOT_URL:="http://${CONTAINER_HOST}:${CONTAINER_PORT}"}

# Тестовое сообщение для проверки журналирования.
# @link https://company.atlassian.net/wiki/spaces/dev/pages/254542134/HTTP
LOGGING_TEST_TEXT=${LOGGING_TEST_TEXT:='This is a test message'}

# Переменные окруженния, которые следует пропускать при проверке.
SKIP_ENV_VARS=" ${SKIP_ENV_VARS:=''} " # Краевые пробелы важны!

##[ Внутренние переменные ]#########################################################################

# Имя последней проверки (см. функции check*).
__lastCheckName=''

####################################################################################################

##
# Выводит сообщение об ошибке.
#
# @param ${1} Сообщение об ошибке.
#
function printError
{
    local message="${1}"

    echo -e "\e[31m${message}\e[0m" >/dev/stderr
}

##
# Выводит сообщение об ошибке и завершает сценарий.
#
# @param ${1} Сообщение об ошибке.
# @param ${2} Код завершения (необязательно).
#
function fatalError
{
    local message="${1}"
    local exitCode=${2:-129}

    printError "${message}"
    exit ${exitCode}
}

##
# Преобразовывает список переменных окружения в список аргументов для docker.
#
# @param ${*} Имена переменных
#
function dockerComposeEnvArgs
{
    for envVar in ${*}; do
        echo -n " -e ${envVar}=''"
    done
}

##
# Возвращает состояние контейнера.
#
# @param ${1} Имя контейнера.
#
function dockerContainerStatus
{
    local container="${1?'Не указано имя контейнера!'}"

    docker inspect -f '{{.State.Status}}' "${container}"
}

##
# Удаляет контейнер Docker.
#
# @param ${1} Имя контейнера.
#
function dockerRemoveContainer
{
    local container="${1?'Не указано имя контейнера!'}"

    docker rm --force --volumes "${container}" 1>/dev/null 2>/dev/null
}

##
# Запускает контейнер Docker.
#
# @param ${1} Имя контейнера.
# @param ...  Дополнительные аргументы для docker run.
#
function dockerRunContainer
{
    local container="${1?'Не указано имя контейнера!'}"
    shift

    docker run --detach --name "${container}" --publish "${CONTAINER_PORT}:${SERVICE_PORT}" ${CONTAINER_RUN_ARGS} $@ "${DOCKER_IMAGE}" 1>/dev/null 2>/dev/null
}

##
# Ожидает когда запустится контейнер.
#
# @param ${1} URL для проверки соединения.
#
function dockerWaitContainerUp
{
    local url="${1?'Не указан URL для проверки соединения!'}"

    limit=10
    # Для проверки используем запрос HTTP, а не простую проверку открытости порта, т. к. Docker
    # открывает порт при запуске контейнера, но приложение может и не слушать на этом порту.
    while ! curl "${url}" 1>/dev/null 2>/dev/null; do
        sleep 1
        ((limit--))
        if [[ ${limit} -eq 0 ]]; then
            curl --include --verbose "${url}"
            fatalError 'Не удалось подключиться к контейнеру!'
        fi
    done
}

##
# Ожидает когда контейнер перейдёт в указанное состояние.
#
# @param ${1} Имя контейнера.
# @param ${2} Ожидаемое состояние, например «running».
# @param ${3} ТАйм-аут ожидания в секундах (по умолчанию 5).
#
function dockerWaitContainerStatus
{
    local container="${1?'Не указано имя контейнера!'}"
    local expectedStatus="${2?'Не указано ожидаемое состояние!'}"
    local timeout="${3-5}"

    while [[ "$(dockerContainerStatus "${container}")" != "${expectedStatus}" ]]; do
        sleep 1
        ((timeout--))
        if [[ ${timeout} -eq 0 ]]; then
            actualStatus=$(dockerContainerStatus "${container}")
            printError "Контейнер не перешёл в состояние \"${expectedStatus}\", текущее состояние \"${actualStatus}\"."
            return 1
        fi
    done
}

##
# Загружает переменные окружения из файлов «.env», «.ebv.dist».
#
# @param ${1} Путь к корневой папке проекта.
#
function envVarsFromDotEnv
{
    local rootDir="${1?'Не указан путь к корневой папке проекта'}"

    local envFile=''
    if [[ -f "${rootDir}/.env" ]]; then
        envFile="${rootDir}/.env"
    elif [[ -f "${rootDir}/.env.dist" ]]; then
        envFile="${rootDir}/.env.dist"
    fi

    if [[ "${envFile}" != '' ]]; then
        while IFS= read -r line; do
            # Удаляем комментарии.
            line="${line%%#*}"
            # Удаляем концевые пробелы.
            line="${line%% *}"
            if [[ "${line}" != '' ]]; then
                echo "${line}"
            fi
        done < "${envFile}"
    fi
}

##
# Выполняет инструмент разработчика.
#
# @param ${1} Команда.
# @param ...  Аргументы.
#
function runDevTool
{
    docker-compose run dev-tools "${@}"
}

##
# Показывает сообщение о начале проверки.
#
# @param ${1} Название проверки.
#
function checkStart
{
    local checkName="${1?'Не указано название проверки!'}"

    __lastCheckName="${checkName}"
    echo -n " ‣ ${checkName}"
}

##
# Показывает сообщение о пропуске проверки.
#
function checkSkipped
{
    echo -e "\r \e[33m➖\e[0m ${__lastCheckName} (пропущено)"
}

##
# Показывает сообщение об успешном выполнении проверки.
#
function checkSucceeded
{
    echo -e "\r \e[32m✔\e[0m ${__lastCheckName}"
}

##
# Показывает сообщение о провале проверки.
#
# @param ${1} Необязательное описание ошибки.
#
function checkFailed
{
    local errorInfo="${1}"

    echo -ne "\r \e[31m✘\e[0m ${__lastCheckName}"

    if [[ "${errorInfo}" != '' ]]; then
        echo -n ": ${errorInfo}"
    fi
    echo
}
