## PHP

- [src/](../src) — исходные коды и ресурсы.
- `tests/`
  - [Functional](../tests/Functional) — функциональные тесты.
  - [Integration](../tests/Integration) — интеграционные тесты.
  - [Unit](../tests/Unit) — модульные тесты.
- [composer.json](../composer.json) — конфигурация [Composer](http://getcomposer.org/).
- [composer.lock](../composer.lock) — версии используемых зависимостей[^1].

[^1]: https://getcomposer.org/doc/01-basic-usage.md#commit-your-composer-lock-file-to-version-control
