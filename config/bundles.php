<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle::class => ['all' => true],
    Company\JsonApiBundle\CompanyJsonApiBundle::class => ['all' => true],
    Company\PHPUnit\Symfony\CompanyTestsBundle::class => ['test' => true],
    Company\SecurityBundle\CompanySecurityBundle::class => ['all' => true],
    Company\ServiceBundle\CompanyServiceBundle::class => ['all' => true],
    Company\S2SBundle\CompanyS2SBundle::class => ['all' => true],
];
